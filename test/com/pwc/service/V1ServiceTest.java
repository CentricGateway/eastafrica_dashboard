/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pwc.service;

import com.cgw.service.V1Service;
import com.google.gson.GsonBuilder;
import com.cgw.service.request.model.CreateMerchantRequest;
import com.cgw.service.response.model.BaseResponse;
import com.cgw.service.response.model.ChargeResponse;
import javax.servlet.http.HttpServletRequest;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

 
public class V1ServiceTest {
    
    public V1ServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of Charge method, of class V1Service.
     */
    @Test
    public void testCharge() {
        System.out.println("Charge");
        CreateMerchantRequest createMerchantRequest = new CreateMerchantRequest();
        createMerchantRequest.setPublickey("pubkey2");
        createMerchantRequest.getMerchantaddress().setAddressline1("thomas");
        createMerchantRequest.getMerchantaddress().setAddressline2("estate");
        createMerchantRequest.getMerchantaddress().setCity("lagos");
        createMerchantRequest.getMerchantaddress().setCountry("NG");
        createMerchantRequest.getMerchantaddress().setState("lagos");
        createMerchantRequest.getMerchantaddress().setTimezone("UTF+1");
        createMerchantRequest.getMerchantinfo().setBusinessname("s and s");
        createMerchantRequest.getMerchantinfo().setEmail("o@s.com");
        createMerchantRequest.getMerchantinfo().setPhonenumber("080");
        createMerchantRequest.getMerchantinfo().setRcnumber("Rc001");
        createMerchantRequest.getMerchantinfo().setWebsite("www.ff.com");
        createMerchantRequest.getUserinfo().setFirstname("hen");
        createMerchantRequest.getUserinfo().setLastname("hry");
        createMerchantRequest.getUserinfo().setMobile("080");
        createMerchantRequest.getUserinfo().setPassword("pass");
        createMerchantRequest.getUserinfo().setUsername("kk@gh.com");
        HttpServletRequest requestContext = null;
        V1Service instance = new V1Service();
        ChargeResponse expResult = null;
        BaseResponse result = instance.CreateMerchant(createMerchantRequest, requestContext);
        
        
        System.out.println(new GsonBuilder().create().toJson(result));
    }
    
}
