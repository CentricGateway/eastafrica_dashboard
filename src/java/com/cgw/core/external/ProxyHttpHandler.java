/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cgw.core.external;

import com.cgw.core.external.model.PostResponse;
import java.io.IOException;
import java.security.cert.X509Certificate;
import java.util.List;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLContextBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
 
public class ProxyHttpHandler {

    private static final Logger log = LogManager.getLogger(ProxyHttpHandler.class);

    private static final NeverRetryHandler NEVER_RETRY_HANDLER = new NeverRetryHandler();

    private final int connecttimeout;
    private final int readtimeout;

    public ProxyHttpHandler(int connecttimeout, int readtimeout) {
        this.connecttimeout = connecttimeout;
        this.readtimeout = readtimeout;
    }

    public PostResponse PostRequestWithString(List<NameValuePair> requestParameters, String url) {

        PostResponse postResponse = new PostResponse();
        CloseableHttpClient httpClient = null;
        try {

            HttpPost request = new HttpPost(url);
            request.addHeader("content-type", "application/x-www-form-urlencoded");
            request.setEntity(new UrlEncodedFormEntity(requestParameters, "utf-8"));
            RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(connecttimeout)
                    .setConnectTimeout(connecttimeout)
                    .setConnectionRequestTimeout(readtimeout)
                    .build();

            httpClient = HttpClients.custom().setDefaultRequestConfig(requestConfig).
                    setRetryHandler(NEVER_RETRY_HANDLER).build();
            HttpResponse response = httpClient.execute(request);

            postResponse.setHttpresponsecode(response.getStatusLine().getStatusCode());
            HttpEntity entity = response.getEntity();
            postResponse.setResponsestring(EntityUtils.toString(entity, "UTF-8"));
            log.info("responseString=" + postResponse.getResponsestring());

        } catch (Exception ex) {
            log.error(ex, ex);
            postResponse.setHttpresponsecode(500);
        } finally {
            try {
                if (httpClient != null) {

                    httpClient.close();
                }

            } catch (IOException ex) {
                log.error(ex, ex);
            }
        }
        return postResponse;

    }

    public PostResponse PostRequestWithJson(String json, String url, boolean ignoressl) {

        PostResponse postResponse = new PostResponse();
        CloseableHttpClient httpClient = null;
        try {

            HttpPost request = new HttpPost(url);
            request.addHeader("content-type", "application/json");
            request.setEntity(new StringEntity(json));

            RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(connecttimeout)
                    .setConnectTimeout(connecttimeout)
                    .setConnectionRequestTimeout(readtimeout)
                    .build();

            if (ignoressl) {
                httpClient = HttpClients.custom().setDefaultRequestConfig(requestConfig).
                        setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE).
                        setRetryHandler(NEVER_RETRY_HANDLER).
                    setSslcontext(new SSLContextBuilder().loadTrustMaterial(null, (X509Certificate[] arg0, String arg1) -> true).build()).build();
            } else {
                httpClient = HttpClients.custom().setDefaultRequestConfig(requestConfig).
                        setRetryHandler(NEVER_RETRY_HANDLER).build();

            }
            HttpResponse response = httpClient.execute(request);

            postResponse.setHttpresponsecode(response.getStatusLine().getStatusCode());
            HttpEntity entity = response.getEntity();
            postResponse.setResponsestring(EntityUtils.toString(entity, "UTF-8"));
            log.info("responseString=" + postResponse.getResponsestring());

        } catch (Exception ex) {
            log.error(ex, ex);
            postResponse.setHttpresponsecode(500);
        } finally {
            try {
                if (httpClient != null) {

                    httpClient.close();
                }

            } catch (IOException ex) {
                log.error(ex, ex);
            }
        }
        return postResponse;

    }

    public PostResponse GetRequest(String url) {

        PostResponse postResponse = new PostResponse();
        CloseableHttpClient httpClient = null;
        try {

            HttpGet request = new HttpGet(url);

            RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(connecttimeout)
                    .setConnectTimeout(connecttimeout)
                    .setConnectionRequestTimeout(readtimeout)
                    .build();

            httpClient = HttpClients.custom().setDefaultRequestConfig(requestConfig).
                    setRetryHandler(NEVER_RETRY_HANDLER).build();
            HttpResponse response = httpClient.execute(request);

            postResponse.setHttpresponsecode(response.getStatusLine().getStatusCode());
            HttpEntity entity = response.getEntity();
            postResponse.setResponsestring(EntityUtils.toString(entity, "UTF-8"));
            log.info("responseString=" + postResponse.getResponsestring());

        } catch (Exception ex) {
            log.error(ex, ex);
            postResponse.setHttpresponsecode(500);
        } finally {
            try {
                if (httpClient != null) {

                    httpClient.close();
                }

            } catch (IOException ex) {
                log.error(ex, ex);
            }
        }
        return postResponse;

    }

    public PostResponse PostRequestWithJson(String url) {

        PostResponse postResponse = new PostResponse();
        CloseableHttpClient httpClient = null;
        try {

            HttpPost request = new HttpPost(url);

            RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(connecttimeout)
                    .setConnectTimeout(connecttimeout)
                    .setConnectionRequestTimeout(readtimeout)
                    .build();

            httpClient = HttpClients.custom().setDefaultRequestConfig(requestConfig).
                    setRetryHandler(NEVER_RETRY_HANDLER).build();
            HttpResponse response = httpClient.execute(request);

            postResponse.setHttpresponsecode(response.getStatusLine().getStatusCode());
            HttpEntity entity = response.getEntity();
            postResponse.setResponsestring(EntityUtils.toString(entity, "UTF-8"));
            log.info("responseString=" + postResponse.getResponsestring());

        } catch (Exception ex) {
            log.error(ex, ex);
            postResponse.setHttpresponsecode(500);
        } finally {
            try {
                if (httpClient != null) {

                    httpClient.close();
                }

            } catch (IOException ex) {
                log.error(ex, ex);
            }
        }
        return postResponse;

    }

    public PostResponse PostRequestWithXml(String xml, String url) {

        log.info("xml=" + xml);

        PostResponse postResponse = new PostResponse();
        CloseableHttpClient httpClient = null;
        try {

            HttpPost request = new HttpPost(url);
            request.addHeader("content-type", "text/xml");
            request.setEntity(new StringEntity(xml));

            RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(connecttimeout)
                    .setConnectTimeout(connecttimeout)
                    .setConnectionRequestTimeout(readtimeout)
                    .build();

            httpClient = HttpClients.custom().setDefaultRequestConfig(requestConfig).
                    setRetryHandler(NEVER_RETRY_HANDLER).build();
            HttpResponse response = httpClient.execute(request);

            postResponse.setHttpresponsecode(response.getStatusLine().getStatusCode());
            HttpEntity entity = response.getEntity();
            postResponse.setResponsestring(EntityUtils.toString(entity, "UTF-8"));
            log.info("responseString=" + postResponse.getResponsestring());

        } catch (Exception ex) {
            log.error(ex, ex);
            postResponse.setHttpresponsecode(500);
        } finally {
            try {
                if (httpClient != null) {

                    httpClient.close();
                }

            } catch (IOException ex) {
                log.error(ex, ex);
            }
        }
        return postResponse;

    }

    public PostResponse PostRequestWithXml(String xml, String url, String soapaction) {

        log.info("xml=" + xml);
        log.info("url=" + url);
        log.info("soapaction=" + soapaction);
        PostResponse postResponse = new PostResponse();
        CloseableHttpClient httpClient = null;
        try {

            HttpPost request = new HttpPost(url);
            request.addHeader("content-type", "text/xml");
            request.addHeader("SOAPAction", soapaction);
            request.setEntity(new StringEntity(xml));

            RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(connecttimeout)
                    .setConnectTimeout(connecttimeout)
                    .setConnectionRequestTimeout(readtimeout)
                    .build();

            httpClient = HttpClients.custom().setDefaultRequestConfig(requestConfig).
                    setRetryHandler(NEVER_RETRY_HANDLER).build();
            HttpResponse response = httpClient.execute(request);

            postResponse.setHttpresponsecode(response.getStatusLine().getStatusCode());
            HttpEntity entity = response.getEntity();
            postResponse.setResponsestring(EntityUtils.toString(entity, "UTF-8"));
            log.info("responseString=" + postResponse.getResponsestring());

        } catch (Exception ex) {
            log.error(ex, ex);
            postResponse.setHttpresponsecode(500);
        } finally {
            try {
                if (httpClient != null) {

                    httpClient.close();
                }

            } catch (IOException ex) {
                log.error(ex, ex);
            }
        }
        return postResponse;

    }

    private static class NeverRetryHandler implements org.apache.http.client.HttpRequestRetryHandler {

        private NeverRetryHandler() {
        }

        @Override
        public boolean retryRequest(IOException ioe, int i, HttpContext hc) {
            return false;
        }
    }

}
