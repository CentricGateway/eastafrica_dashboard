/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cgw.core.external.model;

 
public class PostResponse {
    
    private int httpresponsecode;
    private String responsestring;

    /**
     * @return the httpresponsecode
     */
    public int getHttpresponsecode() {
        return httpresponsecode;
    }

    /**
     * @param httpresponsecode the httpresponsecode to set
     */
    public void setHttpresponsecode(int httpresponsecode) {
        this.httpresponsecode = httpresponsecode;
    }

    /**
     * @return the responsestring
     */
    public String getResponsestring() {
        return responsestring;
    }

    /**
     * @param responsestring the responsestring to set
     */
    public void setResponsestring(String responsestring) {
        this.responsestring = responsestring;
    }
    
}