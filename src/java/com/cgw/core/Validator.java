/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cgw.core;

import com.cgw.core.persist.TranslatePersistence;
import com.cgw.dao.Merchant;
import com.cgw.dao.Merchantkey;
import com.cgw.utils.ResponseCodes;
import com.cgw.utils.SourceOperations;
import java.math.BigDecimal;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.BigDecimalValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

 
public class Validator {
    private final Logger log = LogManager.getLogger(Validator.class);

    public static String ProcessValues(String[] paramnames, String... paramvalues) {
        String response = "";

        for (int i = 0; i < paramvalues.length; i++) {
            if (StringUtils.isBlank(paramvalues[i])) {
                response = ResponseCodes.NS5.getMessage() + ": " + paramnames[i];
            }

        }

        return response;
    }

    public static void main(String[] args) {
        System.out.println(SourceOperations.getByCode("mm-charge1"));
    }

    public Merchant ValidateMerchant(String publickey, String authCredentials) {
        String secretkey = authCredentials.replaceFirst("Bearer"
                + " ", "");
        Merchant merchant = new TranslatePersistence().GetMerchant(publickey, secretkey);
        return merchant;

    }

    public static BigDecimal ValidateAmount(String value) {

        BigDecimal amount = new BigDecimalValidator().validate(value);
        if (amount != null) {
            if (amount.compareTo(BigDecimal.ZERO) < 1) {
                return null;
            } else {
                return amount;
            }

        } else {

            return null;

        }

    }

    public static String ValidateCurrency(String currency) {

        switch (currency) {

            case "UGX":
                return currency;

            default:
                return null;
        }

    }

    public static String ValidateChargeCountry(String country) {

        switch (country) {

            case "UG":
                return country;

            default:
                return null;
        }

    }

    public static String ValidatePayoutCountry(String country) {

        switch (country) {

            case "UG":
                return country;
            case "KE":
                return country;

            default:
                return null;
        }

    }

    public Merchantkey ValidateAPiCall(String publickey, String authCredentials) {
        String secretkey = authCredentials.replaceFirst("Bearer"
                + " ", "");
        Merchantkey merchant = new TranslatePersistence().GetApiCaller(publickey, secretkey);
        return merchant;

    }

    public Merchantkey ValidateAPiDashCall(String authCredentials) {
        String secretkey = authCredentials.replaceFirst("Bearer"
                + " ", "");
        Merchantkey merchant = null;
        List<Merchantkey> rr = new TranslatePersistence().GetMerchantByToken(secretkey);
        if (!rr.isEmpty()) {
            log.info("getType="+rr.get(0).getType());
            if ("DASH_API_KEY".equals(rr.get(0).getType())) {
                merchant = rr.get(0);
            }
        }
        return merchant;

    }

    public Merchantkey ValidateAPiCall(String authCredentials) {
        String secretkey = authCredentials.replaceFirst("Bearer"
                + " ", "");
        Merchantkey merchant = null;
        List<Merchantkey> rr = new TranslatePersistence().GetMerchantByToken(secretkey);
        if (!rr.isEmpty()) {
            merchant = rr.get(0);
        }
        return merchant;

    }

}
