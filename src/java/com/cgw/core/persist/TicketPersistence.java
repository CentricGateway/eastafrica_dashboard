/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cgw.core.persist;

import com.cgw.dao.Merchantkey;
import com.cgw.dao.Vasairtimeticket;
import com.cgw.dao.Vasairtimeticketresponse;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.StandardBasicTypes;

 
public class TicketPersistence {

    private static final Logger LOG = LogManager.getLogger(TicketPersistence.class);

    public boolean logGenericTransaction(Object obj) {

        Session session = null;
        Transaction tx = null;
        boolean ret = false;
        try {
            session = NewHibernateUtil.getSessionFactory().getCurrentSession();
            tx = session.beginTransaction();
            session.saveOrUpdate(obj);
            tx.commit();
            ret = true;

            if (session.isOpen()) {
                session.close();
            }

        } catch (HibernateException e) {
            tx.rollback();
            //LoggingUtil.logError(e, logger);
            throw e;
        } catch (Exception e) {
            tx.rollback();
            //LoggingUtil.logError(e, logger);
            LOG.error(e.getMessage());
        } finally {
            try {
                if (session != null) {
                    if ((session.isOpen())) {
                        session.close();
                    }
                }
            } catch (Exception e) {
                LOG.error(e.getMessage());
            }
        }
        return ret;
    }
    
    public static void main(String[] args) {
        System.out.println("87155F3C1FA1E243BC253E572A9C5B1D".toLowerCase());
    }

    public VasairtimeticketList GetTicketById(String ticketid, Merchantkey merchantkey, int perpage, int requestedpage,
            String status, String date, String transactionreference) {
        Session session = null;
        try {
            session = NewHibernateUtil.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            Criteria criteria = session.createCriteria(Vasairtimeticket.class)
                    .add(Restrictions.sqlRestriction("userid = ?", merchantkey.getId(), StandardBasicTypes.INTEGER));

            if (StringUtils.isNotBlank(ticketid)) {
                criteria.add(Restrictions.sqlRestriction("uniqueid = ?", ticketid, StandardBasicTypes.STRING));
            }
            if (StringUtils.isNotBlank(transactionreference)) {
                criteria.add(Restrictions.sqlRestriction("transactionreference = ?", transactionreference, StandardBasicTypes.STRING));
            }
            if (StringUtils.isNotBlank(status)) {
                criteria.add(Restrictions.sqlRestriction("tstatus = ?", status, StandardBasicTypes.STRING));
            }

            if (StringUtils.isNotBlank(date)) {
                try {
                    Date d = new SimpleDateFormat("yyyy-MM-dd").parse(date);
                    criteria.add(Restrictions.sqlRestriction("date(timein)=?", d, StandardBasicTypes.DATE));

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
            long recordTotal = ((Long) criteria.setProjection(Projections.rowCount()).uniqueResult());
            criteria.setProjection(null);
            criteria.addOrder(Order.desc("id"));
            criteria.setFirstResult((requestedpage - 1) * perpage);
            criteria.setMaxResults(perpage);
            List<Vasairtimeticket> results = criteria.list();

            return new VasairtimeticketList(results, recordTotal);

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            if (session != null) {
                if (session.isOpen()) {
                    session.close();
                }
            }

        }

    }

    public List<Vasairtimeticketresponse> GetTicketResponsesByTicketId(String ticketid) {
        List<Vasairtimeticketresponse> ls = new ArrayList<>();
        Session session = null;

        try {
            session = NewHibernateUtil.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            ls = session.createQuery("FROM Vasairtimeticketresponse where ticketid=:ticketid")
                    .setParameter("ticketid", ticketid)
                    .list();

        } catch (Exception e) {
            LOG.error(e, e);
        } finally {
            if (session != null) {
                if (session.isOpen()) {
                    session.close();
                }
            }
        }
        return ls;
    }

    public List<Vasairtimeticket> GetTicketByTicketId(String ticketid) {
        List<Vasairtimeticket> ls = new ArrayList<>();
        Session session = null;

        try {
            session = NewHibernateUtil.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            ls = session.createQuery("FROM Vasairtimeticket where uniqueid=:ticketid")
                    .setParameter("ticketid", ticketid)
                    .list();

        } catch (Exception e) {
            LOG.error(e, e);
        } finally {
            if (session != null) {
                if (session.isOpen()) {
                    session.close();
                }
            }
        }
        return ls;
    }

    public class VasairtimeticketList {

        public List<Vasairtimeticket> transactions;
        public long totalcount;

        public VasairtimeticketList(List<Vasairtimeticket> transactions, long totalcount) {
            this.transactions = transactions;
            this.totalcount = totalcount;
        }

    }

}
