/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cgw.core.persist;

import com.cgw.dao.Charge;
import com.cgw.dao.Merchantinfo;
import com.cgw.dao.Merchantkey;
import com.cgw.dao.Submerchant;
import com.cgw.dao.Vasairtime;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.StandardBasicTypes;

public class ChargePersistence {

    private static final Logger LOG = LogManager.getLogger(ChargePersistence.class);

    public Charge GetChargeByInternalReference(String internalreference) {
        Charge cachedata = null;
        Session session = null;

        try {
            session = NewHibernateUtil.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            List<Charge> ls = session.createQuery("FROM Charge where internalreference=:internalreference")
                    .setParameter("internalreference", internalreference)
                    .list();
            if (!ls.isEmpty()) {
                cachedata = ls.get(0);
            }
        } catch (Exception e) {
            LOG.error(e, e);
        } finally {
            if (session != null) {
                if (session.isOpen()) {
                    session.close();
                }
            }
        }
        return cachedata;
    }//

    public Charge GetChargeByMerchantReference(String merchantreference) {
        Charge cachedata = null;
        Session session = null;

        try {
            session = NewHibernateUtil.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            List<Charge> ls = session.createQuery("FROM Charge where merchantreference=:merchantreference order by id desc")
                    .setParameter("merchantreference", merchantreference)
                    .list();
            if (!ls.isEmpty()) {
                cachedata = ls.get(0);
            }
        } catch (Exception e) {
            LOG.error(e, e);
        } finally {
            if (session != null) {
                if (session.isOpen()) {
                    session.close();
                }
            }
        }

        return cachedata;

    }//

    public boolean logGenericTransaction(Object obj) {

        Session session = null;
        Transaction tx = null;
        boolean ret = false;
        try {
            session = NewHibernateUtil.getSessionFactory().getCurrentSession();
            tx = session.beginTransaction();
            session.saveOrUpdate(obj);
            tx.commit();
            ret = true;

            if (session.isOpen()) {
                session.close();
            }

        } catch (HibernateException e) {
            tx.rollback();
            //LoggingUtil.logError(e, logger);
            throw e;
        } catch (Exception e) {
            tx.rollback();
            //LoggingUtil.logError(e, logger);
            LOG.error(e.getMessage());
        } finally {
            try {
                if (session != null) {
                    if ((session.isOpen())) {
                        session.close();
                    }
                }
            } catch (Exception e) {
                LOG.error(e.getMessage());
            }
        }
        return ret;
    }

    public static void main(String[] args) {
        int pageSize = 4;
        Session session = NewHibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        /*String hql = "FROM Charge f order by id desc";
        Query query = session.createQuery(hql);
        

        ScrollableResults resultScroll = query.scroll(ScrollMode.FORWARD_ONLY);
        resultScroll.first();
        resultScroll.scroll(0);
        List<Charge> fooPage = new ArrayList();
        int i = 0;
        while (pageSize > i++) {
            fooPage.add((Charge) resultScroll.get(0));
            if (!resultScroll.next()) {
                break;
            }
        }
        resultScroll.last();
        int totalResults = resultScroll.getRowNumber() + 1;*/

        Criteria criteria = session.createCriteria(Charge.class);
        criteria.setFirstResult(1);
        criteria.setMaxResults(pageSize);
        List<Charge> firstPage = criteria.list();

        Criteria criteriaCount = session.createCriteria(Charge.class);
        criteriaCount.setProjection(Projections.rowCount());
        Long count = (Long) criteriaCount.uniqueResult();

    }

    public ChargeList GetChargeTransactions(Merchantkey merchantkey, int perpage, int requestedpage, String date, String merchantreference, String internalreference,
            String currency, String cardtype, String transactionstatus, String transactiontype, String todate) {

        Session session = null;
        try {
            session = NewHibernateUtil.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            Criteria criteria = session.createCriteria(Charge.class)
                    .add(Restrictions.sqlRestriction("apikeyid = ?", merchantkey.getId(), StandardBasicTypes.INTEGER));

            if (StringUtils.isNotBlank(todate) && StringUtils.isNotBlank(date)) {
                try {
                    Date dtodate = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(todate + " 23:59:59");
                    Date dfromdate = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(date + " 00:00:01");
                    criteria.add(Restrictions.sqlRestriction("timein>?", dfromdate, StandardBasicTypes.DATE));
                    criteria.add(Restrictions.sqlRestriction("timein<?", dtodate, StandardBasicTypes.DATE));

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else if (StringUtils.isNotBlank(date)) {
                try {
                    Date d = new SimpleDateFormat("yyyy-MM-dd").parse(date);
                    criteria.add(Restrictions.sqlRestriction("date(timein)=?", d, StandardBasicTypes.DATE));

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            if (StringUtils.isNotBlank(merchantreference)) {
                criteria.add(Restrictions.sqlRestriction("merchantreference = ?", merchantreference, StandardBasicTypes.STRING));
            }
            if (StringUtils.isNotBlank(transactiontype)) {
                criteria.add(Restrictions.sqlRestriction("type = ?", transactiontype, StandardBasicTypes.STRING));
            }

            if (StringUtils.isNotBlank(internalreference)) {
                criteria.add(Restrictions.sqlRestriction("internalreference = ?", internalreference, StandardBasicTypes.STRING));
            }
            if (StringUtils.isNotBlank(currency)) {
                criteria.add(Restrictions.sqlRestriction("currency = ?", currency, StandardBasicTypes.STRING));
            }
            if (StringUtils.isNotBlank(transactionstatus)) {
                criteria.add(Restrictions.sqlRestriction("responsecode = ?", transactionstatus, StandardBasicTypes.STRING));
            }
            if (StringUtils.isNotBlank(cardtype)) {
                criteria.add(Restrictions.sqlRestriction("network = ?", cardtype, StandardBasicTypes.STRING));
            }

            long recordTotal = ((Long) criteria.setProjection(Projections.rowCount()).uniqueResult());
            criteria.setProjection(null);
            criteria.addOrder(Order.desc("id"));
            criteria.setFirstResult((requestedpage - 1) * perpage);
            criteria.setMaxResults(perpage);
            List<Charge> results = criteria.list();
            return new ChargeList(results, recordTotal);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            if (session != null) {
                if (session.isOpen()) {
                    session.close();
                }
            }

        }

    }

    public AirtimeList GetAirtimeTransactions(Merchantkey merchantkey, int perpage, int requestedpage, String date,
            String recipient,
            String merchantreference,
            String internalreference,
            String processorreference,
            String responsecode,
            String status,
            String operator) {

        Session session = null;

        try {
            session = NewHibernateUtil.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            Criteria criteria = session.createCriteria(Vasairtime.class);
            LOG.info("getType="+merchantkey.getType());
            if ("STAGING_KEY_SUBMERCHANT".equals(merchantkey.getType())) {
                criteria.add(Restrictions.sqlRestriction("apikeyid = ?", merchantkey.getId(), StandardBasicTypes.INTEGER));
            }

            if (StringUtils.isNotBlank(date)) {
                try {
                    Date d = new SimpleDateFormat("yyyy-MM-dd").parse(date);
                    criteria.add(Restrictions.sqlRestriction("date(timein)=?", d, StandardBasicTypes.DATE));

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
            if (StringUtils.isNotBlank(recipient)) {
                criteria.add(Restrictions.sqlRestriction("mobile = ?", recipient, StandardBasicTypes.STRING));
            }
            if (StringUtils.isNotBlank(merchantreference)) {
                criteria.add(Restrictions.sqlRestriction("merchantreference = ?", merchantreference, StandardBasicTypes.STRING));
            }
            if (StringUtils.isNotBlank(internalreference)) {
                criteria.add(Restrictions.sqlRestriction("uniquereference = ?", internalreference, StandardBasicTypes.STRING));
            }
            if (StringUtils.isNotBlank(processorreference)) {
                criteria.add(Restrictions.sqlRestriction("apitransactionid = ?", processorreference, StandardBasicTypes.STRING));
            }
            if (StringUtils.isNotBlank(responsecode)) {
                criteria.add(Restrictions.sqlRestriction("status = ?", responsecode, StandardBasicTypes.STRING));
            }
            if (StringUtils.isNotBlank(status)) {
                criteria.add(Restrictions.sqlRestriction("code = ?", status, StandardBasicTypes.STRING));
            }
            if (StringUtils.isNotBlank(operator)) {
                criteria.add(Restrictions.sqlRestriction("operatorname = ?", operator, StandardBasicTypes.STRING));
            }
            long recordTotal = ((Long) criteria.setProjection(Projections.rowCount()).uniqueResult());
            criteria.setProjection(null);
            criteria.addOrder(Order.desc("id"));
            criteria.setFirstResult((requestedpage - 1) * perpage);
            criteria.setMaxResults(perpage);
            List<Vasairtime> results = criteria.list();
            return new AirtimeList(results, recordTotal);

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            if (session != null) {
                if (session.isOpen()) {
                    session.close();
                }
            }

        }

    }

    public SubmerchantList GetSubmerchants(Merchantinfo merchantinfoid, int perpage, int requestedpage) {

        Session session = null;
        try {
            session = NewHibernateUtil.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            Criteria criteria = session.createCriteria(Submerchant.class)
                    .add(Restrictions.sqlRestriction("merchantinfoid = ?", merchantinfoid.getId(), StandardBasicTypes.INTEGER));
            long recordTotal = ((Long) criteria.setProjection(Projections.rowCount()).uniqueResult());
            criteria.setProjection(null);
            criteria.addOrder(Order.desc("id"));
            criteria.setFirstResult((requestedpage - 1) * perpage);
            criteria.setMaxResults(perpage);
            List<Submerchant> results = criteria.list();
            return new SubmerchantList(results, recordTotal);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            if (session != null) {
                if (session.isOpen()) {
                    session.close();
                }
            }

        }

    }

    public class ChargeList {

        public List<Charge> transactions;
        public long totalcount;

        public ChargeList(List<Charge> transactions, long totalcount) {
            this.transactions = transactions;
            this.totalcount = totalcount;
        }

    }

    public class SubmerchantList {

        public List<Submerchant> transactions;
        public long totalcount;

        public SubmerchantList(List<Submerchant> transactions, long totalcount) {
            this.transactions = transactions;
            this.totalcount = totalcount;
        }

    }

    public class AirtimeList {

        public List<Vasairtime> transactions;
        public long totalcount;

        public AirtimeList(List<Vasairtime> transactions, long totalcount) {
            this.transactions = transactions;
            this.totalcount = totalcount;
        }

    }
}
