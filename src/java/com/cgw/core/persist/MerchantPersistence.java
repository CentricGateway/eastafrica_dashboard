/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cgw.core.persist;

import com.cgw.dao.Merchantcontact;
import com.cgw.dao.Merchantinfo;
import com.cgw.dao.Merchantkey;
import com.cgw.dao.Merchantuser;
import com.cgw.dao.Submerchantuser;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.transform.Transformers;

 
public class MerchantPersistence {

    private static final Logger LOG = LogManager.getLogger(TranslatePersistence.class);

    public boolean logGenericTransaction(Object obj) {

        Session session = null;
        Transaction tx = null;
        boolean ret = false;
        try {
            session = NewHibernateUtil.getSessionFactory().getCurrentSession();
            tx = session.beginTransaction();
            session.saveOrUpdate(obj);
            tx.commit();
            ret = true;

            if (session.isOpen()) {
                session.close();
            }

        } catch (HibernateException e) {
            tx.rollback();
            //LoggingUtil.logError(e, logger);
            throw e;
        } catch (Exception e) {
            tx.rollback();
            //LoggingUtil.logError(e, logger);
            LOG.error(e.getMessage());
        } finally {
            try {
                if (session != null) {
                    if ((session.isOpen())) {
                        session.close();
                    }
                }
            } catch (Exception e) {
                LOG.error(e.getMessage());
            }
        }
        return ret;
    }

    public boolean GetMerchantByContact(String email, String phone) {
        boolean exist = false;
        Session session = null;

        try {
            session = NewHibernateUtil.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            List<Merchantcontact> ls = session.createQuery("FROM Merchantcontact where email=:email or phonenumber=:phone")
                    .setParameter("email", email)
                    .setParameter("phone", phone)
                    .list();
            if (!ls.isEmpty()) {
                exist = true;
            }
        } catch (Exception e) {
            LOG.error(e, e);
        } finally {
            if (session != null) {
                if (session.isOpen()) {
                    session.close();
                }
            }
        }

        return exist;

    }//

    public Merchantuser GetMerchantByActivationCode(String activationcode) {
        Merchantuser exist = null;
        Session session = null;

        try {
            session = NewHibernateUtil.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            exist = (Merchantuser) session.createQuery("FROM Merchantuser where validationcode=:activationcode")
                    .setParameter("activationcode", activationcode).uniqueResult();

        } catch (Exception e) {
            LOG.error(e, e);
        } finally {
            if (session != null) {
                if (session.isOpen()) {
                    session.close();
                }
            }
        }

        return exist;

    }//

    public Merchantuser AuthenticateUser(String username, String password) {
        Merchantuser exist = null;
        Session session = null;

        try {
            session = NewHibernateUtil.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            List<Merchantuser> ls = session.createQuery("FROM Merchantuser where username=:username and"
                    + " password=:password and validated='1' and status='1'")
                    .setParameter("username", username)
                    .setParameter("password", password)
                    .list();
            if (!ls.isEmpty()) {
                exist = ls.get(0);
            }
        } catch (Exception e) {
            LOG.error(e, e);
        } finally {
            if (session != null) {
                if (session.isOpen()) {
                    session.close();
                }
            }
        }

        return exist;

    }//
    
    public Submerchantuser AuthenticateSubUser(String username, String password) {
        Submerchantuser exist = null;
        Session session = null;

        try {
            session = NewHibernateUtil.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            List<Submerchantuser> ls = session.createQuery("FROM Submerchantuser where username=:username and"
                    + " password=:password and validated='1' and status='1'")
                    .setParameter("username", username)
                    .setParameter("password", password)
                    .list();
            if (!ls.isEmpty()) {
                exist = ls.get(0);
            }
        } catch (Exception e) {
            LOG.error(e, e);
        } finally {
            if (session != null) {
                if (session.isOpen()) {
                    session.close();
                }
            }
        }

        return exist;

    }//

    public  List GetObjects(String query, String[] params,String[] values) {
        Session session = NewHibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        Query query1= session.createQuery(query);
        if (params!=null) {
            for (int i = 0; i < params.length; i++) {
                query1.setParameter(params[i], values[i]);
            }
        }

        return query1.list();
    }

    public static void main(String[] args) {
        List<Merchantuser> list = new MerchantPersistence().
                GetObjects("FROM Merchantuser where username=:uname",new String[]{"uname"},new String[]{"rr@er.com"});

        // List<Object> objectList = new ArrayList<Object>(stringList);
    }
    
    public Merchantkey GetMerchantKeyBymerchantUser(Merchantuser merchantuser) {
        Merchantkey exist = null;
        Session session = null;

        try {
            session = NewHibernateUtil.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            List<Merchantkey> ls = session.createQuery("FROM Merchantkey where userid=:userid")
                    .setParameter("userid", merchantuser.getId())
                    .list();
            if (!ls.isEmpty()) {
                exist = ls.get(0);
            }
        } catch (Exception e) {
            LOG.error(e, e);
        } finally {
            if (session != null) {
                if (session.isOpen()) {
                    session.close();
                }
            }
        }

        return exist;

    }//
    public Merchantkey GetMerchantKeyBymerchantUser(Submerchantuser merchantuser) {
        Merchantkey exist = null;
        Session session = null;

        try {
            session = NewHibernateUtil.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            List<Merchantkey> ls = session.createQuery("FROM Merchantkey where userid=:userid")
                    .setParameter("userid", merchantuser.getId())
                    .list();
            if (!ls.isEmpty()) {
                exist = ls.get(0);
            }
        } catch (Exception e) {
            LOG.error(e, e);
        } finally {
            if (session != null) {
                if (session.isOpen()) {
                    session.close();
                }
            }
        }

        return exist;

    }//
    
    
     public Merchantinfo GetMerchantByMerchantKey(Merchantkey merchantkey) {
        Merchantinfo exist = null;
        Session session = null;

        try {
            session = NewHibernateUtil.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            List<Merchantinfo> ls = session.createSQLQuery("select mi.* from merchantkey mk, merchantuser mu, merchantinfo mi "
                    + " where mk.userid=mu.id and mi.id=mu.merchantid and mk.id=:keyid")
                    .setParameter("keyid", merchantkey.getId()).setResultTransformer(Transformers.aliasToBean(Merchantinfo.class))
                    .list();
            if (!ls.isEmpty()) {
                exist = ls.get(0);
            }
        } catch (Exception e) {
            LOG.error(e, e);
        } finally {
            if (session != null) {
                if (session.isOpen()) {
                    session.close();
                }
            }
        }

        return exist;

    }//
     
     
       public Merchantuser GetMerchantUserByMerchantKey(Merchantkey merchantkey) {
        Merchantuser exist = null;
        Session session = null;

        try {
            session = NewHibernateUtil.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            List<Merchantuser> ls = session.createSQLQuery("select mu.* from merchantkey mk,merchantuser mu where mk.userid=mu.id and mk.id=:keyid")
                    .setParameter("keyid", merchantkey.getId()).setResultTransformer(Transformers.aliasToBean(Merchantuser.class))
                    .list();
            if (!ls.isEmpty()) {
                exist = ls.get(0);
            }
        } catch (Exception e) {
            LOG.error(e, e);
        } finally {
            if (session != null) {
                if (session.isOpen()) {
                    session.close();
                }
            }
        }

        return exist;

    }//


}
