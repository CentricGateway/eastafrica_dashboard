/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cgw.core.persist.model;

import java.util.Date;

 
public class MerchantDetails {

    /**
     * @return the firstname
     */
    public String getFirstname() {
        return firstname;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @return the lastlivestatus
     */
    public Integer getLastlivestatus() {
        return lastlivestatus;
    }

    /**
     * @return the lastlogin
     */
    public Date getLastlogin() {
        return lastlogin;
    }

    /**
     * @return the lastname
     */
    public String getLastname() {
        return lastname;
    }

    /**
     * @return the livestatus
     */
    public Integer getLivestatus() {
        return livestatus;
    }

    /**
     * @return the merchantid
     */
    public Integer getMerchantid() {
        return merchantid;
    }

    /**
     * @return the mobile
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @return the privatekey
     */
    public String getPrivatekey() {
        return privatekey;
    }

    /**
     * @return the publickey
     */
    public String getPublickey() {
        return publickey;
    }

    /**
     * @return the status
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * @return the token
     */
    public String getToken() {
        return token;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @return the userid
     */
    public Integer getUserid() {
        return userid;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @return the userrole
     */
    public Integer getUserrole() {
        return userrole;
    }

    /**
     * @return the validated
     */
    public Integer getValidated() {
        return validated;
    }

    /**
     * @return the validatedon
     */
    public Date getValidatedon() {
        return validatedon;
    }

    /**
     * @return the validationcode
     */
    public String getValidationcode() {
        return validationcode;
    }

    /**
     * @param firstname the firstname to set
     */
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @param lastlivestatus the lastlivestatus to set
     */
    public void setLastlivestatus(Integer lastlivestatus) {
        this.lastlivestatus = lastlivestatus;
    }

    /**
     * @param lastlogin the lastlogin to set
     */
    public void setLastlogin(Date lastlogin) {
        this.lastlogin = lastlogin;
    }

    /**
     * @param lastname the lastname to set
     */
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    /**
     * @param livestatus the livestatus to set
     */
    public void setLivestatus(Integer livestatus) {
        this.livestatus = livestatus;
    }

    /**
     * @param merchantid the merchantid to set
     */
    public void setMerchantid(Integer merchantid) {
        this.merchantid = merchantid;
    }

    /**
     * @param mobile the mobile to set
     */
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @param privatekey the privatekey to set
     */
    public void setPrivatekey(String privatekey) {
        this.privatekey = privatekey;
    }

    /**
     * @param publickey the publickey to set
     */
    public void setPublickey(String publickey) {
        this.publickey = publickey;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * @param token the token to set
     */
    public void setToken(String token) {
        this.token = token;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @param userid the userid to set
     */
    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @param userrole the userrole to set
     */
    public void setUserrole(Integer userrole) {
        this.userrole = userrole;
    }

    /**
     * @param validated the validated to set
     */
    public void setValidated(Integer validated) {
        this.validated = validated;
    }

    /**
     * @param validatedon the validatedon to set
     */
    public void setValidatedon(Date validatedon) {
        this.validatedon = validatedon;
    }

    /**
     * @param validationcode the validationcode to set
     */
    public void setValidationcode(String validationcode) {
        this.validationcode = validationcode;
    }

    private Integer id;
    private Integer userid;
    private String name;
    private String type;
    private String publickey;
    private String privatekey;
    private String token;
    private Integer status;
    private Integer merchantid;
    private String username;
    private String password;
    private Integer userrole;
    private Integer validated;
    private Date validatedon;
    private String validationcode;
    private String firstname;
    private String lastname;
    private String mobile;
    private Date lastlogin;
    private Integer livestatus;
    private Integer lastlivestatus;

}
