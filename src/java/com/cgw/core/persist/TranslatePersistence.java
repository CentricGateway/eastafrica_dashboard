/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cgw.core.persist;

import com.cgw.core.persist.model.MerchantDetails;
import com.cgw.dao.Merchant;
import com.cgw.dao.Merchantinfo;
import com.cgw.dao.Merchantkey;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.transform.Transformers;

 
public class TranslatePersistence {

    private static final Logger LOG = LogManager.getLogger(TranslatePersistence.class);

    public Merchant GetMerchant(String publickey) {
        Merchant cachedata = null;
        Session session = null;

        try {
            session = NewHibernateUtil.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            List<Merchant> ls = session.createQuery("FROM Merchant where publickey=:publickey and status=1")
                    .setParameter("publickey", publickey)
                    .list();
            if (!ls.isEmpty()) {
                cachedata = ls.get(0);
            }
        } catch (Exception e) {
            LOG.error(e, e);
        } finally {
            if (session != null) {
                if (session.isOpen()) {
                    session.close();
                }
            }
        }

        return cachedata;

    }//

    public Merchant GetMerchant(String publickey, String privatekey) {
        Merchant cachedata = null;
        Session session = null;

        try {
            session = NewHibernateUtil.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            List<Merchant> ls = session.createQuery("FROM Merchant where publickey=:publickey and privatekey=:privatekey and status=1")
                    .setParameter("publickey", publickey)
                    .setParameter("privatekey", privatekey)
                    .list();
            if (!ls.isEmpty()) {
                cachedata = ls.get(0);
            }
        } catch (Exception e) {
            LOG.error(e, e);
        } finally {
            if (session != null) {
                if (session.isOpen()) {
                    session.close();
                }
            }
        }

        return cachedata;

    }//

    public boolean logGenericTransaction(Object obj) {

        Session session = null;
        Transaction tx = null;
        boolean ret = false;
        try {
            session = NewHibernateUtil.getSessionFactory().getCurrentSession();
            tx = session.beginTransaction();
            session.saveOrUpdate(obj);
            tx.commit();
            ret = true;

            if (session.isOpen()) {
                session.close();
            }

        } catch (HibernateException e) {
            tx.rollback();
            //LoggingUtil.logError(e, logger);
            throw e;
        } catch (Exception e) {
            tx.rollback();
            //LoggingUtil.logError(e, logger);
            LOG.error(e.getMessage());
        } finally {
            try {
                if (session != null) {
                    if ((session.isOpen())) {
                        session.close();
                    }
                }
            } catch (Exception e) {
                LOG.error(e.getMessage());
            }
        }
        return ret;
    }

    public Merchantkey GetApiCaller(String publickey, String privatekey) {
        Merchantkey cachedata = null;
        Session session = null;

        try {
            session = NewHibernateUtil.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            List<Merchantkey> ls = session.createQuery("FROM Merchantkey where publickey=:publickey and privatekey=:privatekey and status=1")
                    .setParameter("publickey", publickey)
                    .setParameter("privatekey", privatekey)
                    .list();
            if (!ls.isEmpty()) {
                cachedata = ls.get(0);
            }
        } catch (Exception e) {
            LOG.error(e, e);
        } finally {
            if (session != null) {
                if (session.isOpen()) {
                    session.close();
                }
            }
        }

        return cachedata;

    }//

    public List<Merchantkey> GetMerchantByToken(String token) {
        List<Merchantkey> menu = new ArrayList<>();
        Session session = null;

        try {

            session = NewHibernateUtil.getSessionFactory().getCurrentSession();
            session.beginTransaction();

            menu = session.createSQLQuery("SELECT m.* FROM merchanttoken mt,merchantkey m "
                    + " where expires > NOW() and mt.token=:token "
                    + " and m.id=mt.merchantkeyid ")
                    .setParameter("token", token)
                    .setResultTransformer(Transformers.aliasToBean(Merchantkey.class))
                    .setMaxResults(1)
                    .list();

        } catch (Exception | ExceptionInInitializerError e) {
            e.printStackTrace();
        } finally {
            if (session != null) {
                if (session.isOpen()) {
                    session.close();
                }
            }
        }

        return menu;

    }

    public List<Merchantkey> GetMerchantDetailsByToken(String token) {
        List<Merchantkey> menu = new ArrayList<>();
        Session session = null;

        try {

            session = NewHibernateUtil.getSessionFactory().getCurrentSession();
            session.beginTransaction();

            menu = session.createSQLQuery("SELECT m.* FROM merchanttoken mt,merchantkey m "
                    + " where expires > NOW() and mt.token=:token "
                    + " and m.id=mt.merchantkeyid ")
                    .setParameter("token", token)
                    .setResultTransformer(Transformers.aliasToBean(Merchantkey.class))
                    .setMaxResults(1)
                    .list();

        } catch (Exception | ExceptionInInitializerError e) {
            e.printStackTrace();
        } finally {
            if (session != null) {
                if (session.isOpen()) {
                    session.close();
                }
            }
        }

        return menu;

    }

    public List<MerchantDetails> GetFullMerchantDetailsByToken(String keyid) {
        List<MerchantDetails> menu = new ArrayList<>();
        Session session = null;

        try {

            session = NewHibernateUtil.getSessionFactory().getCurrentSession();
            session.beginTransaction();

            menu = session.createSQLQuery("SELECT mk.id	,"
                    + "userid,"
                    + "name,"
                    + "type,"
                    + "publickey,"
                    + "privatekey,"
                    + "token,"
                    + "mk.status,"
                    + "merchantid,"
                    + "username,"
                    + "password,"
                    + "userrole,"
                    + "validated,"
                    + "validatedon,"
                    + "validationcode,"
                    + "firstname,"
                    + "lastname,"
                    + "mobile,"
                    + "lastlogin,"
                    + "livestatus,"
                    + "lastlivestatus	 FROM sbit.merchantkey mk,merchantuser mu where mu.id=mk.userid"
                    + " and mk.id=:keyid")
                    .setParameter("keyid", keyid)
                    .setResultTransformer(Transformers.aliasToBean(MerchantDetails.class))
                    .setMaxResults(1)
                    .list();

        } catch (Exception | ExceptionInInitializerError e) {
            e.printStackTrace();
        } finally {
            if (session != null) {
                if (session.isOpen()) {
                    session.close();
                }
            }
        }

        return menu;

    }

    public List<Merchantinfo> GetMerchantInfoByApiKeyId(String keyid) {
        List<Merchantinfo> menu = new ArrayList<>();
        Session session = null;

        try {

            session = NewHibernateUtil.getSessionFactory().getCurrentSession();
            session.beginTransaction();

            menu = session.createSQLQuery("select m.* from merchantkey mk ,merchantuser mu,merchantinfo m "
                    + " where mu.id=mk.userid and "
                    + " m.id=mu.merchantid "
                    + " and mk.id=:keyid ")
                    .setParameter("keyid", keyid)
                    .setResultTransformer(Transformers.aliasToBean(Merchantinfo.class))
                    .setMaxResults(1)
                    .list();

        } catch (Exception | ExceptionInInitializerError e) {
            e.printStackTrace();
        } finally {
            if (session != null) {
                if (session.isOpen()) {
                    session.close();
                }
            }
        }

        return menu;

    }

}
