/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cgw.core.persist;

import com.cgw.dao.Merchantkey;
import com.cgw.dao.Payout;
import java.math.BigInteger;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
  
public class PayoutPersistence {

    private static final Logger LOG = LogManager.getLogger(PayoutPersistence.class);

    public Payout GetChargeByInternalReference(String internalreference) {
        Payout cachedata = null;
        Session session = null;

        try {
            session = NewHibernateUtil.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            List<Payout> ls = session.createQuery("FROM Payout where internalreference=:internalreference")
                    .setParameter("internalreference", internalreference)
                    .list();
            if (!ls.isEmpty()) {
                cachedata = ls.get(0);
            }
        } catch (Exception e) {
            LOG.error(e, e);
        } finally {
            if (session != null) {
                if (session.isOpen()) {
                    session.close();
                }
            }
        }

        return cachedata;

    }//

    public Payout GetChargeByMerchantReference(String merchantreference) {
        Payout cachedata = null;
        Session session = null;

        try {
            session = NewHibernateUtil.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            List<Payout> ls = session.createQuery("FROM Payout where merchantreference=:merchantreference order by id desc")
                    .setParameter("merchantreference", merchantreference)
                    .list();
            if (!ls.isEmpty()) {
                cachedata = ls.get(0);
            }
        } catch (Exception e) {
            LOG.error(e, e);
        } finally {
            if (session != null) {
                if (session.isOpen()) {
                    session.close();
                }
            }
        }

        return cachedata;

    }//

    public boolean logGenericTransaction(Object obj) {

        Session session = null;
        Transaction tx = null;
        boolean ret = false;
        try {
            session = NewHibernateUtil.getSessionFactory().getCurrentSession();
            tx = session.beginTransaction();
            session.saveOrUpdate(obj);
            tx.commit();
            ret = true;

            if (session.isOpen()) {
                session.close();
            }

        } catch (HibernateException e) {
            tx.rollback();
            //LoggingUtil.logError(e, logger);
            throw e;
        } catch (Exception e) {
            tx.rollback();
            //LoggingUtil.logError(e, logger);
            LOG.error(e.getMessage());
        } finally {
            try {
                if (session != null) {
                    if ((session.isOpen())) {
                        session.close();
                    }
                }
            } catch (Exception e) {
                LOG.error(e.getMessage());
            }
        }
        return ret;
    }

    public PayoutList GetPayoutTransactions(Merchantkey merchantkey, int perpage, int requestedpage) {

        Session session = null;
        /* Session session = NewHibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        Criteria criteria = session.createCriteria(Payout.class)
                .add(Restrictions.sqlRestriction("apikeyid = ?", merchantkey.getId(), StandardBasicTypes.INTEGER));
        criteria.setFirstResult(requestedpage);
        criteria.setMaxResults(perpage);

        List<Payout> firstPage = criteria.list();
        Criteria criteriaCount = session.createCriteria(Payout.class)
                .add(Restrictions.sqlRestriction("apikeyid = ?", merchantkey.getId(), StandardBasicTypes.INTEGER));
        criteriaCount.setProjection(Projections.rowCount());
        Long count = (Long) criteriaCount.uniqueResult();*/
        try {
            session = NewHibernateUtil.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            Criteria criteria = session.createCriteria(Payout.class)
                    .add(Restrictions.sqlRestriction("apikeyid = ?", merchantkey.getId(), StandardBasicTypes.INTEGER));
            long recordTotal = ((Long) criteria.setProjection(Projections.rowCount()).uniqueResult());
            criteria.setProjection(null);
            criteria.addOrder(Order.desc("id"));
            criteria.setFirstResult((requestedpage - 1) * perpage);
            criteria.setMaxResults(perpage);
            List<Payout> results = criteria.list();
            //return new Pager<Collect>(pageSize, pageNo, recordTotal, results);

            return new PayoutList(results, recordTotal);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                if (session != null) {
                    if ((session.isOpen())) {
                        session.close();
                    }
                }
            } catch (Exception e) {
                LOG.error(e.getMessage());
            }
        }

    }//

    public PayoutList GetPayoutTransactions(int merchantid, int perpage, int requestedpage, String defaultcountry, String date,
            String internalreference, String merchantreference, String recipient, String status, String currency, String todate) {

        LOG.info("merchantid=" + merchantid);
        LOG.info("perpage=" + perpage);
        LOG.info("defaultcountry=" + defaultcountry);
        LOG.info("defaultcountry=" + defaultcountry);
        LOG.info("date=" + date);
        LOG.info("internalreference=" + internalreference);
        LOG.info("merchantreference=" + merchantreference);
        LOG.info("recipient=" + recipient);
        LOG.info("status=" + status);
        LOG.info("currency=" + currency);
        LOG.info("todate=" + todate);

        Session session = null;
        try {

            String countquery, sqlquery;
            if (StringUtils.isNotBlank(defaultcountry)) {
                countquery = "select count(distinct p.id) from payout p, merchantkey m,merchantuser mu where m.id=p.apikeyid "
                        + " and mu.merchantid=:merchantid and p.country='" + defaultcountry + "'";

                sqlquery = "select distinct p.* from payout p, merchantkey m,merchantuser mu where m.id=p.apikeyid "
                        + " and mu.merchantid=:merchantid and p.country='" + defaultcountry + "' ";

            } else {

                countquery = "select count(distinct p.id) from payout p, merchantkey m,merchantuser mu where m.id=p.apikeyid and mu.merchantid=:merchantid ";
                sqlquery = "select distinct p.* from payout p, merchantkey m,merchantuser mu where m.id=p.apikeyid and mu.merchantid=:merchantid ";

            }
            if (StringUtils.isNotBlank(todate) && (StringUtils.isNotBlank(date))) {
                sqlquery = sqlquery + " and  p.timein >=:date and  p.timein<=:todate  ";
                countquery = countquery + " and p.timein >=:date and  p.timein<=:todate  ";
            } else if (StringUtils.isNotBlank(date)) {
                sqlquery = sqlquery + " and date(p.timein)=:date ";
                countquery = countquery + " and date(p.timein)=:date ";
            }

            if (StringUtils.isNotBlank(internalreference)) {
                sqlquery = sqlquery + " and internalreference=:internalreference ";
                countquery = countquery + " and internalreference=:internalreference ";
            }

            if (StringUtils.isNotBlank(merchantreference)) {
                sqlquery = sqlquery + " and merchantreference=:merchantreference";
                countquery = countquery + " and merchantreference=:merchantreference";
            }

            if (StringUtils.isNotBlank(recipient)) {
                sqlquery = sqlquery + " and recipient=:recipient ";
                countquery = countquery + " and recipient=:recipient ";
            }

            if (StringUtils.isNotBlank(status)) {
                sqlquery = sqlquery + " and responsecode=:status ";
                countquery = countquery + " and responsecode=:status ";
            }
            if (StringUtils.isNotBlank(currency)) {
                sqlquery = sqlquery + " and currency=:currency ";
                countquery = countquery + " and currency=:currency ";
            }

            sqlquery = sqlquery + " order by p.id desc";
            session = NewHibernateUtil.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            Query query = session.createSQLQuery(countquery).setParameter("merchantid", merchantid);
            if (StringUtils.isNotBlank(date)) {
                query.setParameter("date", date);

            }

            if (StringUtils.isNotBlank(todate)) {
                query.setParameter("todate", todate);
            }

            if (StringUtils.isNotBlank(internalreference)) {
                query.setParameter("internalreference", internalreference);
            }

            if (StringUtils.isNotBlank(merchantreference)) {
                query.setParameter("merchantreference", merchantreference);
            }

            if (StringUtils.isNotBlank(recipient)) {
                query.setParameter("recipient", recipient);
            }

            if (StringUtils.isNotBlank(status)) {
                query.setParameter("status", status);
            }

            if (StringUtils.isNotBlank(currency)) {
                query.setParameter("currency", currency);
            }

            long recordTotal = 0L;
            final List<BigInteger> obj = query.list();
            for (BigInteger l : obj) {
                if (l != null) {
                    recordTotal = l.longValue();
                }
            }
            query = session.createSQLQuery(sqlquery).setParameter("merchantid", merchantid).setResultTransformer(Transformers.aliasToBean(Payout.class));
            if (StringUtils.isNotBlank(date)) {
                query.setParameter("date", date);

            }

            if (StringUtils.isNotBlank(todate)) {
                query.setParameter("todate", todate);
            }

            if (StringUtils.isNotBlank(internalreference)) {
                query.setParameter("internalreference", internalreference);
            }

            if (StringUtils.isNotBlank(merchantreference)) {
                query.setParameter("merchantreference", merchantreference);
            }

            if (StringUtils.isNotBlank(recipient)) {
                query.setParameter("recipient", recipient);
            }

            if (StringUtils.isNotBlank(status)) {
                query.setParameter("status", status);
            }

            if (StringUtils.isNotBlank(currency)) {
                query.setParameter("currency", currency);
            }
            query.setFirstResult((requestedpage - 1) * perpage)
                    .setMaxResults(perpage);

            List<Payout> results = query.list();

            return new PayoutList(results, recordTotal);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                if (session != null) {
                    if ((session.isOpen())) {
                        session.close();
                    }
                }
            } catch (Exception e) {
                LOG.error(e.getMessage());
            }
        }

    }//

    public class PayoutList {

        public List<Payout> transactions;
        public long totalcount;

        public PayoutList(List<Payout> transactions, long totalcount) {
            this.transactions = transactions;
            this.totalcount = totalcount;
        }

    }

}
