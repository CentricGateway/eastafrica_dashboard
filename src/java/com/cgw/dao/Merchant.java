package com.cgw.dao;
// Generated Mar 10, 2018 12:37:18 PM by Hibernate Tools 4.3.1



/**
 * Merchant generated by hbm2java
 */
public class Merchant  implements java.io.Serializable {


     private Integer id;
     private String publickey;
     private String privatekey;
     private String name;
     private String code;
     private String country;
     private String shortname;
     private Integer status;

    public Merchant() {
    }

    public Merchant(String publickey, String privatekey, String name, String code, String country, String shortname, Integer status) {
       this.publickey = publickey;
       this.privatekey = privatekey;
       this.name = name;
       this.code = code;
       this.country = country;
       this.shortname = shortname;
       this.status = status;
    }
   
    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    public String getPublickey() {
        return this.publickey;
    }
    
    public void setPublickey(String publickey) {
        this.publickey = publickey;
    }
    public String getPrivatekey() {
        return this.privatekey;
    }
    
    public void setPrivatekey(String privatekey) {
        this.privatekey = privatekey;
    }
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    public String getCode() {
        return this.code;
    }
    
    public void setCode(String code) {
        this.code = code;
    }
    public String getCountry() {
        return this.country;
    }
    
    public void setCountry(String country) {
        this.country = country;
    }
    public String getShortname() {
        return this.shortname;
    }
    
    public void setShortname(String shortname) {
        this.shortname = shortname;
    }
    public Integer getStatus() {
        return this.status;
    }
    
    public void setStatus(Integer status) {
        this.status = status;
    }




}


