package com.cgw.dao;
// Generated 13-Jun-2018 18:13:42 by Hibernate Tools 4.3.1

import java.math.BigDecimal;
import java.util.Date;

/**
 * Payout generated by hbm2java
 */
public class Payout implements java.io.Serializable {

    /**
     * @return the transactionfees
     */
    public String getTransactionfees() {
        return transactionfees;
    }

    /**
     * @param transactionfees the transactionfees to set
     */
    public void setTransactionfees(String transactionfees) {
        this.transactionfees = transactionfees;
    }

    /**
     * @return the ubacomission
     */
    public String getUbacomission() {
        return ubacomission;
    }

    /**
     * @param ubacomission the ubacomission to set
     */
    public void setUbacomission(String ubacomission) {
        this.ubacomission = ubacomission;
    }

    /**
     * @return the cgcomission
     */
    public String getCgcomission() {
        return cgcomission;
    }

    /**
     * @param cgcomission the cgcomission to set
     */
    public void setCgcomission(String cgcomission) {
        this.cgcomission = cgcomission;
    }

    /**
     * @return the tax
     */
    public String getTax() {
        return tax;
    }

    /**
     * @param tax the tax to set
     */
    public void setTax(String tax) {
        this.tax = tax;
    }

    /**
     * @return the senderaccount
     */
    public String getSenderaccount() {
        return senderaccount;
    }

    /**
     * @param senderaccount the senderaccount to set
     */
    public void setSenderaccount(String senderaccount) {
        this.senderaccount = senderaccount;
    }

    /**
     * @return the firstname
     */
    public String getFirstname() {
        return firstname;
    }

    /**
     * @param firstname the firstname to set
     */
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    /**
     * @return the lastname
     */
    public String getLastname() {
        return lastname;
    }

    /**
     * @param lastname the lastname to set
     */
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    private Integer id;
    private Integer apikeyid;
    private String country;
    private String merchantkey;
    private String recipient;
    private BigDecimal amount;
    private String currency;
    private String merchantreference;
    private String narration;
    private String reason;
    private String internalreference;
    private String processorreference;
    private String responsecode;
    private String responsemessage;
    private String network;
    private String type;
    private Date timein;
    private Date timeout;
    private String recipient2;
    private String recipient3;
    private String recipient4;
    private String status;
    private String status1;
    private String processorresponsecode;
    private String processorresponsemessage;
    private String status2;
    private String status3;
    private String status4;
    private String reversestatus;
    private Date reversetime;
    private Integer reverseretry;
    private String issenderstaff;
    private String firstname, lastname, processorbalance, processorbalancecurrency;
    
    private String transactionfees,ubacomission,cgcomission,tax,senderaccount;


    public Payout() {
    }

    public Payout(Integer apikeyid, String country, String merchantkey, String recipient, BigDecimal amount, String currency, String merchantreference, String narration, String internalreference, String processorreference, String responsecode, String responsemessage, String network, String type, Date timein, Date timeout, String recipient2, String recipient3, String recipient4, String status, String status1, String processorresponsecode, String processorresponsemessage, String status2, String status3, String status4, String reversestatus, Date reversetime, Integer reverseretry, String issenderstaff) {
        this.apikeyid = apikeyid;
        this.country = country;
        this.merchantkey = merchantkey;
        this.recipient = recipient;
        this.amount = amount;
        this.currency = currency;
        this.merchantreference = merchantreference;
        this.narration = narration;
        this.internalreference = internalreference;
        this.processorreference = processorreference;
        this.responsecode = responsecode;
        this.responsemessage = responsemessage;
        this.network = network;
        this.type = type;
        this.timein = timein;
        this.timeout = timeout;
        this.recipient2 = recipient2;
        this.recipient3 = recipient3;
        this.recipient4 = recipient4;
        this.status = status;
        this.status1 = status1;
        this.processorresponsecode = processorresponsecode;
        this.processorresponsemessage = processorresponsemessage;
        this.status2 = status2;
        this.status3 = status3;
        this.status4 = status4;
        this.reversestatus = reversestatus;
        this.reversetime = reversetime;
        this.reverseretry = reverseretry;
        this.issenderstaff = issenderstaff;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getApikeyid() {
        return this.apikeyid;
    }

    public void setApikeyid(Integer apikeyid) {
        this.apikeyid = apikeyid;
    }

    public String getCountry() {
        return this.country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getMerchantkey() {
        return this.merchantkey;
    }

    public void setMerchantkey(String merchantkey) {
        this.merchantkey = merchantkey;
    }

    public String getRecipient() {
        return this.recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public BigDecimal getAmount() {
        return this.amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return this.currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getMerchantreference() {
        return this.merchantreference;
    }

    public void setMerchantreference(String merchantreference) {
        this.merchantreference = merchantreference;
    }

    public String getNarration() {
        return this.narration;
    }

    public void setNarration(String narration) {
        this.narration = narration;
    }

    public String getInternalreference() {
        return this.internalreference;
    }

    public void setInternalreference(String internalreference) {
        this.internalreference = internalreference;
    }

    public String getProcessorreference() {
        return this.processorreference;
    }

    public void setProcessorreference(String processorreference) {
        this.processorreference = processorreference;
    }

    public String getResponsecode() {
        return this.responsecode;
    }

    public void setResponsecode(String responsecode) {
        this.responsecode = responsecode;
    }

    public String getResponsemessage() {
        return this.responsemessage;
    }

    public void setResponsemessage(String responsemessage) {
        this.responsemessage = responsemessage;
    }

    public String getNetwork() {
        return this.network;
    }

    public void setNetwork(String network) {
        this.network = network;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getTimein() {
        return this.timein;
    }

    public void setTimein(Date timein) {
        this.timein = timein;
    }

    public Date getTimeout() {
        return this.timeout;
    }

    public void setTimeout(Date timeout) {
        this.timeout = timeout;
    }

    public String getRecipient2() {
        return this.recipient2;
    }

    public void setRecipient2(String recipient2) {
        this.recipient2 = recipient2;
    }

    public String getRecipient3() {
        return this.recipient3;
    }

    public void setRecipient3(String recipient3) {
        this.recipient3 = recipient3;
    }

    public String getRecipient4() {
        return this.recipient4;
    }

    public void setRecipient4(String recipient4) {
        this.recipient4 = recipient4;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus1() {
        return this.status1;
    }

    public void setStatus1(String status1) {
        this.status1 = status1;
    }

    public String getProcessorresponsecode() {
        return this.processorresponsecode;
    }

    public void setProcessorresponsecode(String processorresponsecode) {
        this.processorresponsecode = processorresponsecode;
    }

    public String getProcessorresponsemessage() {
        return this.processorresponsemessage;
    }

    public void setProcessorresponsemessage(String processorresponsemessage) {
        this.processorresponsemessage = processorresponsemessage;
    }

    public String getStatus2() {
        return this.status2;
    }

    public void setStatus2(String status2) {
        this.status2 = status2;
    }

    public String getStatus3() {
        return this.status3;
    }

    public void setStatus3(String status3) {
        this.status3 = status3;
    }

    public String getStatus4() {
        return this.status4;
    }

    public void setStatus4(String status4) {
        this.status4 = status4;
    }

    public String getReversestatus() {
        return this.reversestatus;
    }

    public void setReversestatus(String reversestatus) {
        this.reversestatus = reversestatus;
    }

    public Date getReversetime() {
        return this.reversetime;
    }

    public void setReversetime(Date reversetime) {
        this.reversetime = reversetime;
    }

    public Integer getReverseretry() {
        return this.reverseretry;
    }

    public void setReverseretry(Integer reverseretry) {
        this.reverseretry = reverseretry;
    }

    public String getIssenderstaff() {
        return this.issenderstaff;
    }

    public void setIssenderstaff(String issenderstaff) {
        this.issenderstaff = issenderstaff;
    }

    /**
     * @return the reason
     */
    public String getReason() {
        return reason;
    }

    /**
     * @param reason the reason to set
     */
    public void setReason(String reason) {
        this.reason = reason;
    }

    /**
     * @return the processorbalance
     */
    public String getProcessorbalance() {
        return processorbalance;
    }

    /**
     * @param processorbalance the processorbalance to set
     */
    public void setProcessorbalance(String processorbalance) {
        this.processorbalance = processorbalance;
    }

    /**
     * @return the processorbalancecurrency
     */
    public String getProcessorbalancecurrency() {
        return processorbalancecurrency;
    }

    /**
     * @param processorbalancecurrency the processorbalancecurrency to set
     */
    public void setProcessorbalancecurrency(String processorbalancecurrency) {
        this.processorbalancecurrency = processorbalancecurrency;
    }

}
