package com.cgw.dao;
// Generated Apr 16, 2018 10:26:15 AM by Hibernate Tools 4.3.1

import java.util.Date;

/**
 * Vasairtimeticket generated by hbm2java
 */
public class Vasairtimeticket implements java.io.Serializable {

    /**
     * @return the closedat
     */
    public Date getClosedat() {
        return closedat;
    }

    /**
     * @return the closedby
     */
    public Integer getClosedby() {
        return closedby;
    }

    /**
     * @param closedat the closedat to set
     */
    public void setClosedat(Date closedat) {
        this.closedat = closedat;
    }

    /**
     * @param closedby the closedby to set
     */
    public void setClosedby(Integer closedby) {
        this.closedby = closedby;
    }

    private Integer id;
    private Integer userid;
    private String title, uniqueid,transactionreference;
    private String description;
    private Date timein;
    private String tstatus;
    private Integer closedby;
    private Date closedat;

    public Vasairtimeticket() {
    }

    public Vasairtimeticket(Integer userid, String title, String description, Date timein, String tstatus,String transactionreference) {
        this.userid = userid;
        this.title = title;
        this.description = description;
        this.timein = timein;
        this.tstatus = tstatus;
         this.transactionreference = transactionreference;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserid() {
        return this.userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getTimein() {
        return this.timein;
    }

    public void setTimein(Date timein) {
        this.timein = timein;
    }

    public String getTstatus() {
        return this.tstatus;
    }

    public void setTstatus(String tstatus) {
        this.tstatus = tstatus;
    }

    /**
     * @return the uniqueid
     */
    public String getUniqueid() {
        return uniqueid;
    }

    /**
     * @param uniqueid the uniqueid to set
     */
    public void setUniqueid(String uniqueid) {
        this.uniqueid = uniqueid;
    }

    /**
     * @return the transactionreference
     */
    public String getTransactionreference() {
        return transactionreference;
    }

    /**
     * @param transactionreference the transactionreference to set
     */
    public void setTransactionreference(String transactionreference) {
        this.transactionreference = transactionreference;
    }

}
