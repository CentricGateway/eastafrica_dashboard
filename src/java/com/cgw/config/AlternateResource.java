/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cgw.config;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Locale;
import java.util.ResourceBundle;

 
public class AlternateResource {

 
    private static final String resourcePath = "/var/log/wildfly/config/cgwdash/";
    private static final String resourceName = "config";
    private static final ResourceBundle rsb = GetResourceBundle();
    public final static String HIBERNATE_CONFIG_URL = rsb.getString("HIBERNATE_CONFIG_URL");

    public static String prop(String key) {
        try {
            ResourceBundle rb = GetResourceBundle();
            return rb.getString(key);

        } catch (Exception e) {
            System.out.println(e.getMessage());
            return "Uknown Error";
        }

    }

    private static ResourceBundle GetResourceBundle() {

        ResourceBundle rb = null;
        try {
            File file = new File(resourcePath);
            URL[] urls = {file.toURI().toURL()};
            ClassLoader loader = new URLClassLoader(urls);
            rb = ResourceBundle.getBundle(resourceName, Locale.getDefault(), loader);

        } catch (MalformedURLException ex) {
            ex.printStackTrace();
        }

        return rb;
    }

}
