/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cgw.service.request.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

  
@JsonIgnoreProperties(ignoreUnknown = true)
public class BaseRequest {
    
    private String publickey;

    /**
     * @return the publickey
     */
    public String getPublickey() {
        return publickey;
    }

    /**
     * @param publickey the publickey to set
     */
    public void setPublickey(String publickey) {
        this.publickey = publickey;
    }
    
}
