/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cgw.service.request.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

 
@JsonIgnoreProperties(ignoreUnknown = true)
public class Transaction {
    
    private String reference;
    private String linkingreference;
    

    /**
     * @return the reference
     */
    public String getReference() {
        return reference;
    }

    /**
     * @param reference the reference to set
     */
    public void setReference(String reference) {
        this.reference = reference;
    }

    /**
     * @return the linkingreference
     */
    public String getLinkingreference() {
        return linkingreference;
    }

    /**
     * @param linkingreference the linkingreference to set
     */
    public void setLinkingreference(String linkingreference) {
        this.linkingreference = linkingreference;
    }

     
    
}
