/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cgw.service.request.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

 
@JsonIgnoreProperties(ignoreUnknown = true)
public class CreateMerchantRequest extends BaseRequest {

    private MerchantInfo merchantinfo = new MerchantInfo();
    private UserInfo userinfo = new UserInfo();
    private MerchantAddress merchantaddress = new MerchantAddress();

    /**
     * @return the merchantaddress
     */
    public MerchantAddress getMerchantaddress() {
        return merchantaddress;
    }

    /**
     * @return the merchantinfo
     */
    public MerchantInfo getMerchantinfo() {
        return merchantinfo;
    }

    /**
     * @return the userinfo
     */
    public UserInfo getUserinfo() {
        return userinfo;
    }

    /**
     * @param merchantaddress the merchantaddress to set
     */
    public void setMerchantaddress(MerchantAddress merchantaddress) {
        this.merchantaddress = merchantaddress;
    }

    /**
     * @param merchantinfo the merchantinfo to set
     */
    public void setMerchantinfo(MerchantInfo merchantinfo) {
        this.merchantinfo = merchantinfo;
    }

    /**
     * @param userinfo the userinfo to set
     */
    public void setUserinfo(UserInfo userinfo) {
        this.userinfo = userinfo;
    }

}
