/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cgw.service.request.model;

 
public class AuthenticateUserRequest extends BaseRequest {

     private UserInfo userinfo = new UserInfo();

    /**
     * @return the userinfo
     */
    public UserInfo getUserinfo() {
        return userinfo;
    }

    /**
     * @param userinfo the userinfo to set
     */
    public void setUserinfo(UserInfo userinfo) {
        this.userinfo = userinfo;
    }
    
    
    
}
