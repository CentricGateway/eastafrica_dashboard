/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cgw.service.request.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

 
@JsonIgnoreProperties(ignoreUnknown = true)
public class ChargeRequest extends BaseRequest {
    
    private Transaction transaction=new Transaction(); 
    private Order order=new Order();
    private FundingSource source=new FundingSource();

    /**
     * @return the transaction
     */
    public Transaction getTransaction() {
        return transaction;
    }

    /**
     * @param transaction the transaction to set
     */
    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    /**
     * @return the order
     */
    public Order getOrder() {
        return order;
    }

    /**
     * @param order the order to set
     */
    public void setOrder(Order order) {
        this.order = order;
    }

    /**
     * @return the source
     */
    public FundingSource getSource() {
        return source;
    }

    /**
     * @param source the source to set
     */
    public void setSource(FundingSource source) {
        this.source = source;
    }
    
}
