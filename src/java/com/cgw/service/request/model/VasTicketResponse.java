/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cgw.service.request.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

 
public class VasTicketResponse {

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @return the ticketid
     */
    public String getTicketid() {
        return ticketid;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @param ticketid the ticketid to set
     */
    public void setTicketid(String ticketid) {
        this.ticketid = ticketid;
    }

    @SerializedName("ticketid")
    @Expose
    private String ticketid;
    @SerializedName("description")
    @Expose
    private String description;

   

}
