/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cgw.service.request.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CreateVasTicketResRequest extends BaseRequest {

    @SerializedName("ticket")
    @Expose
    private VasTicketResponse response;

    /**
     * @return the response
     */
    public VasTicketResponse getResponse() {
        return response;
    }

    /**
     * @param response the response to set
     */
    public void setResponse(VasTicketResponse response) {
        this.response = response;
    }

  
}
