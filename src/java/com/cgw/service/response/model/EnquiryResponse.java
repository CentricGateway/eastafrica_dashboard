/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cgw.service.response.model;

import com.cgw.utils.ResponseCodes;

 
public class EnquiryResponse extends BaseResponse {
    
    public EnquiryResponse(){
    
    }
    public EnquiryResponse(ResponseCodes codes){
    
        super(codes);
    }

    private String cutomername;

    /**
     * @return the cutomername
     */
    public String getCutomername() {
        return cutomername;
    }

    /**
     * @param cutomername the cutomername to set
     */
    public void setCutomername(String cutomername) {
        this.cutomername = cutomername;
    }
    
    
}
