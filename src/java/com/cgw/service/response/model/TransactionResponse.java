/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cgw.service.response.model;

 
public class TransactionResponse {

    private String reference;
    private String linkingreference;
    private String providerreference;

    /**
     * @return the linkingreference
     */
    public String getLinkingreference() {
        return linkingreference;
    }

    /**
     * @return the providerreference
     */
    public String getProviderreference() {
        return providerreference;
    }

    /**
     * @param linkingreference the linkingreference to set
     */
    public void setLinkingreference(String linkingreference) {
        this.linkingreference = linkingreference;
    }

    /**
     * @param providerreference the providerreference to set
     */
    public void setProviderreference(String providerreference) {
        this.providerreference = providerreference;
    }

    /**
     * @return the reference
     */
    public String getReference() {
        return reference;
    }

    /**
     * @param reference the reference to set
     */
    public void setReference(String reference) {
        this.reference = reference;
    }

}
