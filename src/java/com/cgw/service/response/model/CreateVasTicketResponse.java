/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cgw.service.response.model;

import com.cgw.utils.ResponseCodes;

 
public class CreateVasTicketResponse extends BaseResponse {

    
    
    public CreateVasTicketResponse(){
    
    }
    public CreateVasTicketResponse(ResponseCodes codes){
    
        super(codes);
    }

    private String ticketid;

    /**
     * @return the ticketid
     */
    public String getTicketid() {
        return ticketid;
    }

    /**
     * @param ticketid the ticketid to set
     */
    public void setTicketid(String ticketid) {
        this.ticketid = ticketid;
    }
  
}
