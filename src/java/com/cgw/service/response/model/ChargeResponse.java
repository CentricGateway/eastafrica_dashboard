/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cgw.service.response.model;

import com.cgw.utils.ResponseCodes;

 
public class ChargeResponse extends BaseResponse {
    
    public ChargeResponse(){
    
    }
    public ChargeResponse(ResponseCodes codes){
    
        super(codes);
    }

    
    private TransactionResponse transaction=new TransactionResponse();

    /**
     * @return the transaction
     */
    public TransactionResponse getTransaction() {
        return transaction;
    }

    /**
     * @param transaction the transaction to set
     */
    public void setTransaction(TransactionResponse transaction) {
        this.transaction = transaction;
    }
    
}
