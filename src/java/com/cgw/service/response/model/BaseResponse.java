/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cgw.service.response.model;

import com.cgw.utils.ResponseCodes;

 
public class BaseResponse {
    
    public BaseResponse(){
    
    }
    public BaseResponse(ResponseCodes codes){
    
        this.code=codes.getCode();
        this.message=codes.getMessage();
    }

    private String code;
    private String message;

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }
    

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

}
