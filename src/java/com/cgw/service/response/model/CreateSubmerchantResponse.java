/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cgw.service.response.model;

import com.cgw.utils.ResponseCodes;

 
public class CreateSubmerchantResponse extends BaseResponse {

    /**
     * @return the apipublickey
     */
    public String getApipublickey() {
        return apipublickey;
    }

    /**
     * @param apipublickey the apipublickey to set
     */
    public void setApipublickey(String apipublickey) {
        this.apipublickey = apipublickey;
    }

    /**
     * @return the apiprivatekey
     */
    public String getApiprivatekey() {
        return apiprivatekey;
    }

    /**
     * @param apiprivatekey the apiprivatekey to set
     */
    public void setApiprivatekey(String apiprivatekey) {
        this.apiprivatekey = apiprivatekey;
    }

    
    public CreateSubmerchantResponse(){
    
    }
    public CreateSubmerchantResponse(ResponseCodes codes){
    
        super(codes);
    }

    
    private String apipublickey,apiprivatekey;

     
    
}
