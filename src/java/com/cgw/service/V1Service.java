/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cgw.service;

import com.cgw.dash.core.DashHandler;
import com.cgw.dash.core.TicketHandler;
import com.cgw.dash.core.report.model.AirtimeTransactionResponse;
import com.cgw.dash.core.report.model.GetSubmerchantsResponse;
import com.cgw.dash.core.report.model.GetVasAirtimeTicketResponse;
import com.cgw.dash.core.report.model.PayoutTransactionResponse;
import com.cgw.service.request.model.AuthenticateUserRequest;
import com.cgw.service.request.model.CloseVasTicketRequest;
import com.cgw.utils.ResponseCodes;
import com.cgw.service.request.model.CreateMerchantRequest;
import com.cgw.service.request.model.CreateVasTicketRequest;
import com.cgw.service.request.model.CreateVasTicketResRequest;
import com.cgw.service.response.model.AuthenticateUserResponse;
import com.cgw.service.response.model.BaseResponse;
import com.cgw.service.response.model.CreateVasTicketResponse;
import com.google.gson.GsonBuilder;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.POST;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

  
@Path("v1")
public class V1Service {

    private final Logger log = LogManager.getLogger(V1Service.class);

    public static final String AUTHENTICATION_HEADER = "Authorization";

    @POST
    @Path("merchant/account/create")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public BaseResponse CreateMerchant(CreateMerchantRequest createMerchantRequest,
            @Context HttpServletRequest requestContext) {
        log.info("===========Start merchant/account/create===========");
        log.info("getPublickey=" + createMerchantRequest.getPublickey());
        if (StringUtils.isBlank(createMerchantRequest.getPublickey())) {
            return new BaseResponse(ResponseCodes.NS8);
        }

        String authCredentials = "";

        if (requestContext != null) {
            authCredentials = requestContext.getHeader(AUTHENTICATION_HEADER);
        }

        if (StringUtils.isBlank(authCredentials)) {
            return new BaseResponse(ResponseCodes.NS10);
        }
        BaseResponse info;
        try {
            info = new DashHandler().CreateMerchant(createMerchantRequest, authCredentials);

        } catch (Exception e) {
            log.error(e, e);
            return new BaseResponse(ResponseCodes.NS7);
        }

        log.info("getCode=" + info.getCode());
        log.info("getMessage=" + info.getMessage());
        log.info("===========end merchant/account/create===========");
        return info;

    }

    @GET
    @Path("merchant/account/activate/{activationcode}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public BaseResponse ActivateMerchant(@PathParam("activationcode") String activationcode,
            @Context HttpServletRequest requestContext) {
        log.info("===========Start merchant/account/activate===========");
        log.info("activationcode=" + activationcode);

        BaseResponse info;
        try {
            info = new DashHandler().ActivateMerchant(activationcode);

        } catch (Exception e) {
            log.error(e, e);
            return new BaseResponse(ResponseCodes.NS7);
        }

        log.info("getCode=" + info.getCode());
        log.info("getMessage=" + info.getMessage());
        log.info("===========end merchant/account/activate===========");
        return info;

    }

    @POST
    @Path("merchant/account/authenticate")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public AuthenticateUserResponse AuthenticateMerchant(AuthenticateUserRequest authenticateUserRequest,
            @Context HttpServletRequest requestContext) {
        log.info("===========Start merchant/account/authenticate===========");
        log.info("getPublickey=" + authenticateUserRequest.getPublickey());
        if (StringUtils.isBlank(authenticateUserRequest.getPublickey())) {
            return new AuthenticateUserResponse(ResponseCodes.NS8);
        }

        String authCredentials = "";

        if (requestContext != null) {
            authCredentials = requestContext.getHeader(AUTHENTICATION_HEADER);
        }

        if (StringUtils.isBlank(authCredentials)) {
            return new AuthenticateUserResponse(ResponseCodes.NS10);
        }
        AuthenticateUserResponse info;
        try {
            info = new DashHandler().AuthenticateMerchant(authenticateUserRequest, authCredentials);

        } catch (Exception e) {
            log.error(e, e);
            return new AuthenticateUserResponse(ResponseCodes.NS7);
        }

        log.info("response=" + new GsonBuilder().create().toJson(info));
        log.info("===========end merchant/account/create===========");
        return info;

    }

    //payout/transactions
    @GET
    @Path("payout/transactions")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public PayoutTransactionResponse GetPayoutTransactions(
            @QueryParam("perpage") int perpage,
            @QueryParam("page") int requestedpage,
            @QueryParam("date") String date,
            @QueryParam("internalreference") String internalreference,
            @QueryParam("merchantreference") String merchantreference,
            @QueryParam("recipient") String recipient,
            @QueryParam("status") String status,
            @QueryParam("currency") String currency,
            @QueryParam("todate") String todate,
            @Context HttpServletRequest requestContext) {
        log.info("===========Start payout/transactions===========");
        log.info("page=" + requestedpage);

        String authCredentials = "";

        if (requestContext != null) {
            authCredentials = requestContext.getHeader(AUTHENTICATION_HEADER);
        }

        if (StringUtils.isBlank(authCredentials)) {
            return new PayoutTransactionResponse(ResponseCodes.NS10);
        }
        PayoutTransactionResponse info;
        try {
            info = new DashHandler().GetPayoutTransactions(authCredentials,  perpage, requestedpage, date,
                    internalreference, merchantreference, recipient, status, currency, todate);

        } catch (Exception e) {
            log.error(e, e);
            return new PayoutTransactionResponse(ResponseCodes.NS7);
        }

        log.info("getCode=" + info.getCode());
        log.info("getMessage=" + info.getMessage());
        log.info("===========end payout/transactions===========");
        return info;

    }

    @GET
    @Path("charge/transactions")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public PayoutTransactionResponse GetChargeTransactions(
            @QueryParam("type") String type,
            @QueryParam("date") String date,
            @QueryParam("merchantreference") String merchantreference,
            @QueryParam("internalreference") String internalreference,
            @QueryParam("currency") String currency,
            @QueryParam("status") String transactionstatus,
            @QueryParam("cardtype") String cardtype,
            @QueryParam("perpage") int perpage,
            @QueryParam("page") int requestedpage,
            @QueryParam("todate") String todate,
            @Context HttpServletRequest requestContext) {
        log.info("===========Start charge/transactions===========");
        log.info("transactionreference=" + merchantreference);
        log.info("internalreference=" + internalreference);
        log.info("page=" + requestedpage);

        String authCredentials = "";

        if (requestContext != null) {
            authCredentials = requestContext.getHeader(AUTHENTICATION_HEADER);
        }

        if (StringUtils.isBlank(authCredentials)) {
            return new PayoutTransactionResponse(ResponseCodes.NS10);
        }
        PayoutTransactionResponse info;
        try {
            info = new DashHandler().GetChargeTransactions(authCredentials, perpage, requestedpage,
                    date, merchantreference, internalreference,
                    currency, cardtype, transactionstatus, type, todate);

        } catch (Exception e) {
            log.error(e, e);
            return new PayoutTransactionResponse(ResponseCodes.NS7);
        }

        log.info("getCode=" + info.getCode());
        log.info("getMessage=" + info.getMessage());
        log.info("===========end payout/transactions===========");
        return info;

    }

    @GET
    @Path("airtime/transactions")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public AirtimeTransactionResponse GetAirtimeTransactions(
            @QueryParam("date") String date,
            @QueryParam("recipient") String recipient,
            @QueryParam("merchantreference") String merchantreference,
            @QueryParam("internalreference") String internalreference,
            @QueryParam("processorreference") String processorreference,
            @QueryParam("responsecode") String responsecode,
            @QueryParam("status") String status,
            @QueryParam("operator") String operator,
            @QueryParam("perpage") int perpage,
            @QueryParam("page") int requestedpage,
            @Context HttpServletRequest requestContext) {
        log.info("===========Start airtime/transactions===========");
        log.info("merchantreference=" + merchantreference);
        log.info("internalreference=" + internalreference);
        log.info("recipient=" + recipient);
        log.info("processorreference=" + processorreference);
        log.info("responsecode=" + responsecode);
        log.info("status=" + status);
        log.info("operator=" + operator);
        log.info("perpage=" + perpage);

        log.info("page=" + requestedpage);

        String authCredentials = "";

        if (requestContext != null) {
            authCredentials = requestContext.getHeader(AUTHENTICATION_HEADER);
        }

        if (StringUtils.isBlank(authCredentials)) {
            return new AirtimeTransactionResponse(ResponseCodes.NS10);
        }
        AirtimeTransactionResponse info;
        try {
            info = new DashHandler().GetAirtimeTransactions(authCredentials, perpage, requestedpage,
                    date,
                    recipient,
                    merchantreference,
                    internalreference,
                    processorreference,
                    responsecode,
                    status,
                    operator);

        } catch (Exception e) {
            log.error(e, e);
            return new AirtimeTransactionResponse(ResponseCodes.NS7);
        }

        log.info("response=" + new GsonBuilder().serializeNulls().disableHtmlEscaping().create().toJson(info));
        log.info("===========end airtime/transactions===========");
        return info;

    }

    @POST
    @Path("airtime/ticket/create")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public CreateVasTicketResponse CreateVasAirtimeTicket(CreateVasTicketRequest createMerchantRequest,
            @Context HttpServletRequest requestContext) {
        log.info("===========Start airtime/ticket/create===========");
        log.info("getPublickey=" + createMerchantRequest.getPublickey());
        if (StringUtils.isBlank(createMerchantRequest.getPublickey())) {
            return new CreateVasTicketResponse(ResponseCodes.NS8);
        }

        String authCredentials = "";

        if (requestContext != null) {
            authCredentials = requestContext.getHeader(AUTHENTICATION_HEADER);
        }

        if (StringUtils.isBlank(authCredentials)) {
            return new CreateVasTicketResponse(ResponseCodes.NS10);
        }
        CreateVasTicketResponse info;
        try {
            info = new TicketHandler().CreateTicket(createMerchantRequest, authCredentials);

        } catch (Exception e) {
            log.error(e, e);
            return new CreateVasTicketResponse(ResponseCodes.NS7);
        }

        log.info("getCode=" + info.getCode());
        log.info("getMessage=" + info.getMessage());
        log.info("===========end merchant/account/create===========");
        return info;

    }

    @GET
    @Path("airtime/tickets")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public GetVasAirtimeTicketResponse GetAirtimeTickets(
            @QueryParam("date") String date,
            @QueryParam("ticketid") String ticketid,
            @QueryParam("transactionreference") String transactionreference,
            @QueryParam("status") String status,
            @QueryParam("perpage") int perpage,
            @QueryParam("page") int requestedpage,
            @Context HttpServletRequest requestContext) {
        log.info("===========Start airtime/tickets===========");
        log.info("ticketid=" + ticketid);
        log.info("status=" + status);
        log.info("perpage=" + perpage);
        log.info("page=" + requestedpage);

        String authCredentials = "";

        if (requestContext != null) {
            authCredentials = requestContext.getHeader(AUTHENTICATION_HEADER);
        }

        if (StringUtils.isBlank(authCredentials)) {
            return new GetVasAirtimeTicketResponse(ResponseCodes.NS10);
        }
        GetVasAirtimeTicketResponse info;
        try {
            info = new TicketHandler().
                    GetTicket(ticketid, status, authCredentials, perpage,
                            requestedpage, date, transactionreference);

        } catch (Exception e) {
            log.error(e, e);
            return new GetVasAirtimeTicketResponse(ResponseCodes.NS7);
        }

        log.info("response=" + new GsonBuilder().serializeNulls().disableHtmlEscaping().create().toJson(info));
        log.info("===========end airtime/tickets===========");
        return info;

    }

    @PUT
    @Path("airtime/ticket/response/create")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public CreateVasTicketResponse CreateVasAirtimeTicketResponse(CreateVasTicketResRequest createMerchantRequest,
            @Context HttpServletRequest requestContext) {
        log.info("===========Start airtime/ticket/response/create===========");
        log.info("getPublickey=" + createMerchantRequest.getPublickey());
        if (StringUtils.isBlank(createMerchantRequest.getPublickey())) {
            return new CreateVasTicketResponse(ResponseCodes.NS8);
        }

        String authCredentials = "";

        if (requestContext != null) {
            authCredentials = requestContext.getHeader(AUTHENTICATION_HEADER);
        }

        if (StringUtils.isBlank(authCredentials)) {
            return new CreateVasTicketResponse(ResponseCodes.NS10);
        }
        CreateVasTicketResponse info;
        try {
            info = new TicketHandler().CreateTicketResponse(createMerchantRequest, authCredentials);

        } catch (Exception e) {
            log.error(e, e);
            return new CreateVasTicketResponse(ResponseCodes.NS7);
        }

        log.info("getCode=" + info.getCode());
        log.info("getMessage=" + info.getMessage());
        log.info("===========end  airtime/ticket/response/create===========");
        return info;

    }

    @PUT
    @Path("airtime/ticket/close")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public BaseResponse CloseVasAirtimeTicket(CloseVasTicketRequest createMerchantRequest,
            @Context HttpServletRequest requestContext) {
        log.info("===========Start airtime/ticket/close===========");
        log.info("getPublickey=" + createMerchantRequest.getPublickey());
        if (StringUtils.isBlank(createMerchantRequest.getPublickey())) {
            return new BaseResponse(ResponseCodes.NS8);
        }

        String authCredentials = "";

        if (requestContext != null) {
            authCredentials = requestContext.getHeader(AUTHENTICATION_HEADER);
        }

        if (StringUtils.isBlank(authCredentials)) {
            return new BaseResponse(ResponseCodes.NS10);
        }
        BaseResponse info;
        try {
            info = new TicketHandler().CloseTicket(createMerchantRequest, authCredentials);

        } catch (Exception e) {
            log.error(e, e);
            return new CreateVasTicketResponse(ResponseCodes.NS7);
        }

        log.info("getCode=" + info.getCode());
        log.info("getMessage=" + info.getMessage());
        log.info("===========end merchant/account/close===========");
        return info;

    }

    @POST
    @Path("merchant/subaccount/create")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public BaseResponse CreateSubMerchant(CreateMerchantRequest createMerchantRequest,
            @Context HttpServletRequest requestContext) {
        log.info("===========Start merchant/subaccount/create===========");
        log.info("request=" + new GsonBuilder().serializeNulls().disableHtmlEscaping().create().toJson(createMerchantRequest));
        if (StringUtils.isBlank(createMerchantRequest.getPublickey())) {
            return new BaseResponse(ResponseCodes.NS8);
        }

        String authCredentials = "";

        if (requestContext != null) {
            authCredentials = requestContext.getHeader(AUTHENTICATION_HEADER);
        }

        if (StringUtils.isBlank(authCredentials)) {
            return new BaseResponse(ResponseCodes.NS10);
        }
        BaseResponse info;
        try {
            info = new DashHandler().CreateSubMerchant(createMerchantRequest, authCredentials);

        } catch (Exception e) {
            log.error(e, e);
            return new BaseResponse(ResponseCodes.NS7);
        }

        log.info("getCode=" + info.getCode());
        log.info("getMessage=" + info.getMessage());
        log.info("===========end merchant/subaccount/create===========");
        return info;

    }

    @GET
    @Path("merchant/subaccounts")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public GetSubmerchantsResponse GetSubMerchants(
            @QueryParam("perpage") int perpage,
            @QueryParam("page") int requestedpage,
            @Context HttpServletRequest requestContext) {
        log.info("===========Start merchant/subaccounts===========");

        String authCredentials = "";

        if (requestContext != null) {
            authCredentials = requestContext.getHeader(AUTHENTICATION_HEADER);
        }

        if (StringUtils.isBlank(authCredentials)) {
            return new GetSubmerchantsResponse(ResponseCodes.NS10);
        }
        GetSubmerchantsResponse info;
        try {
            info = new DashHandler().GetSubMerchants(authCredentials, perpage, requestedpage);

        } catch (Exception e) {
            log.error(e, e);
            return new GetSubmerchantsResponse(ResponseCodes.NS7);
        }

        log.info("getCode=" + info.getCode());
        log.info("getMessage=" + info.getMessage());
        log.info("===========end merchant/subaccounts===========");
        return info;

    }

}
