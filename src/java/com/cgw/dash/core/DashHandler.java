/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cgw.dash.core;

import com.cgw.core.Validator;
import com.cgw.core.persist.MerchantPersistence;
import com.cgw.core.persist.TranslatePersistence;
import com.cgw.core.persist.model.MerchantDetails;
import com.cgw.dao.Merchantinfo;
import com.cgw.dao.Merchantaddress;
import com.cgw.dao.Merchantcontact;
import com.cgw.dao.Merchantkey;
import com.cgw.dao.Merchanttoken;
import com.cgw.dao.Merchantuser;
import com.cgw.dao.Submerchant;
import com.cgw.dao.Submerchantuser;
import com.cgw.dash.core.report.DashProcessor;
import com.cgw.dash.core.report.model.AirtimeTransactionResponse;
import com.cgw.dash.core.report.model.GetSubmerchantsResponse;
import com.cgw.dash.core.report.model.PayoutTransactionResponse;
import com.cgw.service.request.model.AuthenticateUserRequest;
import com.cgw.utils.GenericUtils;
import com.cgw.utils.RandomString;
import com.cgw.utils.ResponseCodes;
import com.cgw.utils.SystemUtils;
import com.cgw.service.request.model.CreateMerchantRequest;
import com.cgw.service.response.model.AuthenticateUserResponse;
import com.cgw.service.response.model.BaseResponse;
import com.cgw.service.response.model.CreateSubmerchantResponse;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Response;
import org.apache.commons.lang3.StringUtils;
import org.apache.oltu.oauth2.as.issuer.MD5Generator;
import org.apache.oltu.oauth2.as.issuer.OAuthIssuer;
import org.apache.oltu.oauth2.as.issuer.OAuthIssuerImpl;
import org.apache.oltu.oauth2.as.response.OAuthASResponse;
import org.apache.oltu.oauth2.common.message.OAuthResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
 
public class DashHandler {

    private final int EXPIRY_SECONDS = 86400;

    private final Logger log = LogManager.getLogger(DashHandler.class);

    public BaseResponse CreateMerchant(CreateMerchantRequest createMerchantRequest, String authCredentials) {

        BaseResponse response = new BaseResponse();
        MerchantPersistence merchantPersistence = new MerchantPersistence();

        String resp = Validator.ProcessValues(new String[]{"merchantinfo/email"},
                new String[]{createMerchantRequest.getMerchantinfo().getEmail()});

        String resp2 = Validator.ProcessValues(new String[]{"merchantinfo/businessname"},
                new String[]{createMerchantRequest.getMerchantinfo().getBusinessname()});

        if (StringUtils.isBlank(resp) || StringUtils.isBlank(resp2)) {

            Merchantkey merchantkey = new Validator().ValidateAPiDashCall(authCredentials);
            if (merchantkey == null) {
                return new BaseResponse(ResponseCodes.NS1);
            }

            boolean exist = merchantPersistence.GetMerchantByContact(createMerchantRequest.getMerchantinfo().getEmail(),
                    createMerchantRequest.getMerchantinfo().getPhonenumber());
            if (exist) {
                response.setCode(ResponseCodes.NS12.getCode());
                response.setMessage("Sorry that account already exists");

            } else {
                Merchantinfo merchantinfo = new Merchantinfo();
                merchantinfo.setBusinessname(createMerchantRequest.getMerchantinfo().getBusinessname());
                merchantinfo.setCreatedon(new Date());
                merchantinfo.setLivestatus(0);
                merchantinfo.setName(createMerchantRequest.getMerchantinfo().getBusinessname());
                merchantinfo.setWebsite(createMerchantRequest.getMerchantinfo().getWebsite());
                merchantPersistence.logGenericTransaction(merchantinfo);
                merchantinfo.setCode("M" + String.format("%06d", merchantinfo.getId()));
                merchantPersistence.logGenericTransaction(merchantinfo);
                Merchantaddress merchantAddress = new Merchantaddress();
                merchantAddress.setAddressline1(createMerchantRequest.getMerchantaddress().getAddressline1());
                merchantAddress.setAddressline2(createMerchantRequest.getMerchantaddress().getAddressline2());
                merchantAddress.setCity(createMerchantRequest.getMerchantaddress().getCity());
                merchantAddress.setCountry(createMerchantRequest.getMerchantaddress().getCountry());
                merchantAddress.setMerchantid(merchantinfo.getId());
                merchantAddress.setState(createMerchantRequest.getMerchantaddress().getState());
                merchantAddress.setTimezone(createMerchantRequest.getMerchantaddress().getTimezone());
                merchantPersistence.logGenericTransaction(merchantAddress);
                Merchantcontact merchantcontact = new Merchantcontact();
                merchantcontact.setEmail(createMerchantRequest.getMerchantinfo().getEmail());
                merchantcontact.setMerchantid(merchantinfo.getId());
                merchantcontact.setPhonenumber(createMerchantRequest.getMerchantinfo().getPhonenumber());
                merchantcontact.setWebsite(createMerchantRequest.getMerchantinfo().getWebsite());
                merchantPersistence.logGenericTransaction(merchantcontact);
                Merchantuser merchantuser = new Merchantuser();
                merchantuser.setFirstname(createMerchantRequest.getUserinfo().getFirstname());
                merchantuser.setLastlivestatus(0);
                merchantuser.setLastname(createMerchantRequest.getUserinfo().getLastname());
                merchantuser.setLivestatus(0);
                merchantuser.setMerchantid(merchantinfo.getId());
                merchantuser.setMobile(createMerchantRequest.getUserinfo().getMobile());
                merchantuser.setPassword(GenericUtils.GetSHA512String(createMerchantRequest.getUserinfo().getPassword()));
                merchantuser.setStatus(0);
                merchantuser.setUsername(createMerchantRequest.getUserinfo().getUsername());
                merchantuser.setValidated(0);
                merchantuser.setValidationcode(new RandomString(64, ThreadLocalRandom.current()).nextString() + SystemUtils.GenerateUuid());
                merchantPersistence.logGenericTransaction(merchantuser);
                Merchantkey merchantkey1 = new Merchantkey();
                merchantkey1.setUserid(merchantuser.getId());
                merchantkey1.setName("STAGING_KEY");
                merchantkey1.setPrivatekey("spk_" + new RandomString(25, ThreadLocalRandom.current()).nextString());
                merchantkey1.setPublickey("lpk_" + new RandomString(25, ThreadLocalRandom.current()).nextString());
                merchantkey1.setType("STAGING_KEY");
                merchantPersistence.logGenericTransaction(merchantkey1);
                response.setCode(ResponseCodes.N00.getCode());
                response.setMessage("Registration Successful. Please check the email you provided for a link to activate your account");
            }
        } else {
            response.setCode(ResponseCodes.NS5.getCode());
            response.setMessage(resp + "|" + resp2);
        }
        return response;

    }

    public BaseResponse ActivateMerchant(String activationcode) {

        BaseResponse response = new BaseResponse();
        MerchantPersistence merchantPersistence = new MerchantPersistence();

        String resp = Validator.ProcessValues(new String[]{activationcode},
                new String[]{activationcode});

        if (StringUtils.isBlank(resp)) {

            Merchantuser user = merchantPersistence.GetMerchantByActivationCode(activationcode);
            if (user == null) {
                response.setCode(ResponseCodes.NS12.getCode());
                response.setMessage("Sorry that code is invalid/expired");
            } else {
                user.setValidated(1);
                user.setValidatedon(new Date());
                user.setStatus(1);
                merchantPersistence.logGenericTransaction(user);
                response.setCode(ResponseCodes.N00.getCode());
                response.setMessage("Account successfully activated. Please login");
            }

        } else {
            response.setCode(ResponseCodes.NS5.getCode());
            response.setMessage(resp);
        }

        return response;

    }

    public AuthenticateUserResponse AuthenticateMerchant(
            AuthenticateUserRequest authenticateUserRequest, String authCredentials) {
        AuthenticateUserResponse response = new AuthenticateUserResponse();
        MerchantPersistence merchantPersistence = new MerchantPersistence();

        String resp = Validator.ProcessValues(new String[]{"userinfo/username"},
                new String[]{authenticateUserRequest.getUserinfo().getUsername()});

        String resp2 = Validator.ProcessValues(new String[]{"userinfo/password"},
                new String[]{authenticateUserRequest.getUserinfo().getUsername()});

        if (StringUtils.isBlank(resp) || StringUtils.isBlank(resp2)) {

            Merchantkey merchantkey = new Validator().ValidateAPiDashCall(authCredentials);
            if (merchantkey == null) {
                log.info("APiDashCall merchantkey not found");
                return new AuthenticateUserResponse(ResponseCodes.NS1);
            }

            log.info("merchant authenticated successfully");

            Submerchantuser submerchantuser = null;
            Merchantuser merchantuser = merchantPersistence.
                    AuthenticateUser(authenticateUserRequest.getUserinfo().getUsername(),
                            authenticateUserRequest.getUserinfo().getPassword());
            if (merchantuser == null) {
                submerchantuser = merchantPersistence
                        .AuthenticateSubUser(authenticateUserRequest.getUserinfo().getUsername(),
                                authenticateUserRequest.getUserinfo().getPassword());
                if (submerchantuser == null) {
                    return new AuthenticateUserResponse(ResponseCodes.NS15);
                }

            }

            if (submerchantuser != null && submerchantuser.getValidated() != 1) {
                return new AuthenticateUserResponse(ResponseCodes.NS16);

            } else if (submerchantuser != null && submerchantuser.getStatus() != 1) {
                return new AuthenticateUserResponse(ResponseCodes.NS17);

            } else if (merchantuser != null && merchantuser.getValidated() != 1) {
                return new AuthenticateUserResponse(ResponseCodes.NS16);

            } else if (merchantuser != null && merchantuser.getStatus() != 1) {
                return new AuthenticateUserResponse(ResponseCodes.NS17);

            } else {
                response.setCode(ResponseCodes.N00.getCode());
                response.setMessage("Authentication Successful");
                String token = GenerateOauthToken(merchantuser, submerchantuser);
                response.setAccess_token(token);
                response.setExpires_in(EXPIRY_SECONDS);
                if (submerchantuser != null) {
                    response.setUsertype("submerchant");
                } else {
                    response.setUsertype("merchant");
                }

            }

        } else {
            response.setCode(ResponseCodes.NS5.getCode());
            response.setMessage(resp + "|" + resp2);
        }
        return response;
    }

    private String GenerateOauthToken(Merchantuser merchantuser, Submerchantuser submerchantuser) {

        String accessToken = "";
        try {
            MerchantPersistence merchantPersistence = new MerchantPersistence();
            OAuthIssuer oauthIssuerImpl = new OAuthIssuerImpl(new MD5Generator());

            final String aToken = oauthIssuerImpl.accessToken();
            Merchanttoken merchanttoken = new Merchanttoken();
            merchanttoken.setExpired(0);
            merchanttoken.setCreated(new Date());
            if (submerchantuser != null) {
                log.info("getting user's key now");
                Merchantkey merchantkey = merchantPersistence.GetMerchantKeyBymerchantUser(submerchantuser);
                merchanttoken.setMerchantkeyid(merchantkey.getId());
                merchanttoken.setType("DASH_USER_SUBMERCHANT");

            } else {

                Merchantkey merchantkey = merchantPersistence.GetMerchantKeyBymerchantUser(merchantuser);
                merchanttoken.setMerchantkeyid(merchantkey.getId());
                merchanttoken.setType("DASH_USER");
            }
            merchanttoken.setToken(aToken);

            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.SECOND, EXPIRY_SECONDS);
            merchanttoken.setExpires(calendar.getTime());
            if (new TranslatePersistence().logGenericTransaction(merchanttoken)) {
                accessToken = aToken;
                OAuthResponse response = OAuthASResponse
                        .tokenResponse(HttpServletResponse.SC_OK)
                        .setAccessToken(accessToken)
                        .setExpiresIn(String.valueOf(EXPIRY_SECONDS))
                        .buildJSONMessage();
                Response.status(response.getResponseStatus()).entity(response.getBody()).build();

            } else {
                OAuthResponse response = OAuthResponse
                        .errorResponse(401)
                        .setError("system_error")
                        .setErrorDescription("system error")
                        .buildJSONMessage();
                Response.status(response.getResponseStatus()).entity(response.getBody()).build();
            }
        } catch (Exception ex) {
            log.error(ex, ex);
        }

        return accessToken;
    }

    public PayoutTransactionResponse GetPayoutTransactions(String authCredentials,  int perpage, int requestedpage, String date, String internalreference, String merchantreference, String recipient, String status
            , String currency, String todate) {
        PayoutTransactionResponse baseResponse = new PayoutTransactionResponse();
        Merchantkey merchantkey = new Validator().ValidateAPiCall(authCredentials);
        if (merchantkey == null) {
            return new PayoutTransactionResponse(ResponseCodes.NS1);
        }

        baseResponse = new DashProcessor().GetPayoutTransactions(merchantkey, perpage, requestedpage, date,  internalreference,  merchantreference,  recipient,  status,  currency,  todate);

        return baseResponse;
    }

    public PayoutTransactionResponse GetChargeTransactions(String authCredentials, int perpage, int requestedpage,
             String date, String merchantreference, String internalreference,
            String currency, String cardtype, String transactionstatus, String transactiontype, String todate) {
        PayoutTransactionResponse baseResponse = new PayoutTransactionResponse();
        Merchantkey merchantkey = new Validator().ValidateAPiCall(authCredentials);
        if (merchantkey == null) {
            return new PayoutTransactionResponse(ResponseCodes.NS1);
        }

        baseResponse = new DashProcessor().GetChargeTransactions(merchantkey, perpage, requestedpage, date, merchantreference, internalreference,
                currency, cardtype, transactionstatus, transactiontype, todate);

        return baseResponse;
    }

    public CreateSubmerchantResponse CreateSubMerchant(CreateMerchantRequest createMerchantRequest, String authCredentials) {

        CreateSubmerchantResponse response = new CreateSubmerchantResponse();
        MerchantPersistence merchantPersistence = new MerchantPersistence();

        String resp = Validator.ProcessValues(new String[]{"merchantinfo/email"},
                new String[]{createMerchantRequest.getMerchantinfo().getEmail()});

        String resp2 = Validator.ProcessValues(new String[]{"merchantinfo/businessname"},
                new String[]{createMerchantRequest.getMerchantinfo().getBusinessname()});

        if (StringUtils.isBlank(resp) || StringUtils.isBlank(resp2)) {

            Merchantkey merchantkey = new Validator().ValidateAPiCall(authCredentials);
            if (merchantkey == null) {
                return new CreateSubmerchantResponse(ResponseCodes.NS1);
            }

            boolean exist = merchantPersistence.GetMerchantByContact(createMerchantRequest.getMerchantinfo().getEmail(),
                    createMerchantRequest.getMerchantinfo().getPhonenumber());
            if (exist) {
                response.setCode(ResponseCodes.NS12.getCode());
                response.setMessage("Sorry that account already exists");

            } else {

                List<Merchantinfo> detailses = new TranslatePersistence().GetMerchantInfoByApiKeyId("" + merchantkey.getId());

                Submerchant submerchant = new Submerchant();
                submerchant.setMerchantinfoid(detailses.get(0).getId());
                submerchant.setBusinessname(createMerchantRequest.getMerchantinfo().getBusinessname());
                submerchant.setCreatedon(new Date());
                submerchant.setLivestatus(0);
                submerchant.setName(createMerchantRequest.getMerchantinfo().getBusinessname());
                submerchant.setWebsite(createMerchantRequest.getMerchantinfo().getWebsite());
                merchantPersistence.logGenericTransaction(submerchant);
                submerchant.setCode("M" + String.format("%06d", submerchant.getId()));
                merchantPersistence.logGenericTransaction(submerchant);
                /////create address info
                Merchantaddress merchantAddress = new Merchantaddress();
                merchantAddress.setAddressline1(createMerchantRequest.getMerchantaddress().getAddressline1());
                merchantAddress.setAddressline2(createMerchantRequest.getMerchantaddress().getAddressline2());
                merchantAddress.setCity(createMerchantRequest.getMerchantaddress().getCity());
                merchantAddress.setCountry(createMerchantRequest.getMerchantaddress().getCountry());
                merchantAddress.setMerchantid(submerchant.getId());
                merchantAddress.setState(createMerchantRequest.getMerchantaddress().getState());
                merchantAddress.setTimezone(createMerchantRequest.getMerchantaddress().getTimezone());
                merchantAddress.setType("SUBMERCHANT");
                merchantPersistence.logGenericTransaction(merchantAddress);

                Merchantcontact merchantcontact = new Merchantcontact();
                merchantcontact.setEmail(createMerchantRequest.getMerchantinfo().getEmail());
                merchantcontact.setMerchantid(submerchant.getId());
                merchantcontact.setPhonenumber(createMerchantRequest.getMerchantinfo().getPhonenumber());
                merchantcontact.setWebsite(createMerchantRequest.getMerchantinfo().getWebsite());
                merchantcontact.setType("SUBMERCHANT");
                merchantPersistence.logGenericTransaction(merchantcontact);

                Submerchantuser merchantuser = new Submerchantuser();
                merchantuser.setFirstname(createMerchantRequest.getUserinfo().getFirstname());
                merchantuser.setLastlivestatus(0);
                merchantuser.setLastname(createMerchantRequest.getUserinfo().getLastname());
                merchantuser.setLivestatus(1);
                merchantuser.setSubmerchantid(submerchant.getId());
                merchantuser.setMobile(createMerchantRequest.getUserinfo().getMobile());
                merchantuser.setPassword(GenericUtils.GetSHA512String(createMerchantRequest.getUserinfo().getPassword()));
                merchantuser.setStatus(1);
                merchantuser.setUsername(createMerchantRequest.getUserinfo().getUsername());
                merchantuser.setValidated(1);
                merchantuser.setValidationcode(new RandomString(64, ThreadLocalRandom.current()).nextString() + SystemUtils.GenerateUuid());
                merchantPersistence.logGenericTransaction(merchantuser);
                Merchantkey merchantkey1 = new Merchantkey();
                merchantkey1.setUserid(merchantuser.getId());
                merchantkey1.setName("STAGING_KEY");
                merchantkey1.setPrivatekey("spk_" + new RandomString(25, ThreadLocalRandom.current()).nextString());
                merchantkey1.setPublickey("lpk_" + new RandomString(25, ThreadLocalRandom.current()).nextString());
                merchantkey1.setType("STAGING_KEY_SUBMERCHANT");
                merchantkey1.setTimein(new Date());
                merchantkey1.setStatus(1);
                merchantPersistence.logGenericTransaction(merchantkey1);
                response.setCode(ResponseCodes.N00.getCode());
                response.setMessage("Registration Successful");
                response.setApiprivatekey(merchantkey1.getPrivatekey());
                response.setApipublickey(merchantkey1.getPublickey());
            }
        } else {
            response.setCode(ResponseCodes.NS5.getCode());
            response.setMessage(resp + "|" + resp2);
        }
        return response;

    }

    public AirtimeTransactionResponse GetAirtimeTransactions(String authCredentials,
            int perpage, int requestedpage,
            String date,
            String recipient,
            String merchantreference,
            String internalreference,
            String processorreference,
            String responsecode,
            String status,
            String operator) {
        AirtimeTransactionResponse baseResponse = new AirtimeTransactionResponse();
        Merchantkey merchantkey = new Validator().ValidateAPiCall(authCredentials);
        if (merchantkey == null) {
            return new AirtimeTransactionResponse(ResponseCodes.NS1);
        }

        MerchantPersistence merchantPersistence = new MerchantPersistence();
        Merchantinfo merchantinfo = merchantPersistence.GetMerchantByMerchantKey(merchantkey);

        Merchantuser merchantuser = merchantPersistence.GetMerchantUserByMerchantKey(merchantkey);

        baseResponse = new DashProcessor().GetAirtimeTransactions(merchantkey, perpage, requestedpage,
                date,
                recipient,
                merchantreference,
                internalreference,
                processorreference,
                responsecode,
                status,
                operator);

        return baseResponse;
    }

    public GetSubmerchantsResponse GetSubMerchants(String authCredentials, int perpage, int requestedpage) {
        GetSubmerchantsResponse baseResponse = new GetSubmerchantsResponse();
        Merchantkey merchantkey = new Validator().ValidateAPiCall(authCredentials);
        if (merchantkey == null) {
            return new GetSubmerchantsResponse(ResponseCodes.NS1);
        }

        List<Merchantinfo> merchantinfos = new TranslatePersistence().GetMerchantInfoByApiKeyId(merchantkey.getId() + "");
        baseResponse = new DashProcessor().GetSubMerchants(merchantinfos.get(0), perpage, requestedpage);

        return baseResponse;
    }

}
