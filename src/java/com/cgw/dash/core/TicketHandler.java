/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cgw.dash.core;

import com.cgw.core.Validator;
import com.cgw.core.persist.TicketPersistence;
import com.cgw.core.persist.TranslatePersistence;
import com.cgw.core.persist.model.MerchantDetails;
import com.cgw.dao.Merchantkey;
import com.cgw.dao.Vasairtimeticket;
import com.cgw.dao.Vasairtimeticketresponse;
import com.cgw.dash.core.report.model.VasAirtimeTicketRecord;
import com.cgw.dash.core.report.model.GetVasAirtimeTicketResponse;
import com.cgw.dash.core.report.model.VasAirtimeTicketRespRecord;
import com.cgw.service.request.model.CloseVasTicketRequest;
import com.cgw.service.request.model.CreateVasTicketRequest;
import com.cgw.service.request.model.CreateVasTicketResRequest;
import com.cgw.service.response.model.BaseResponse;
import com.cgw.service.response.model.CreateVasTicketResponse;
import com.cgw.utils.ResponseCodes;
import com.cgw.utils.SystemUtils;
import java.util.Date;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

 
public class TicketHandler {

    private static final Logger log = LogManager.getLogger(TicketHandler.class);

    private final int DEFAULT_PER_PAGE_COUNT = 20;
    private final int DEFAULT_PAGE = 0;

    public CreateVasTicketResponse CreateTicket(CreateVasTicketRequest request, String authCredentials) {
        CreateVasTicketResponse createVasTicketResponse = new CreateVasTicketResponse();
        Merchantkey merchantkey = new Validator().ValidateAPiCall(authCredentials);
        if (merchantkey == null) {
            return new CreateVasTicketResponse(ResponseCodes.NS1);
        }
        String ticketid = SystemUtils.GenerateUuid();
        TicketPersistence persistence = new TicketPersistence();
        Vasairtimeticket vasairtimeticket = new Vasairtimeticket();
        vasairtimeticket.setUniqueid(ticketid);
        vasairtimeticket.setDescription(request.getTicket().getDescription());
        vasairtimeticket.setTimein(new Date());
        vasairtimeticket.setTitle(request.getTicket().getTitle());
        vasairtimeticket.setTstatus("OPEN");
        vasairtimeticket.setUserid(merchantkey.getId());
        vasairtimeticket.setTransactionreference(request.getTicket().getTransactionreference());
        if (persistence.logGenericTransaction(vasairtimeticket)) {
            createVasTicketResponse.setCode(ResponseCodes.N00.getCode());
            createVasTicketResponse.setMessage(ResponseCodes.N00.getMessage());
            createVasTicketResponse.setTicketid(ticketid);
        } else {

            createVasTicketResponse.setCode(ResponseCodes.NS7.getCode());
            createVasTicketResponse.setMessage("Sorry, your ticket could not be created. Please try again");
        }

        return createVasTicketResponse;
    }

    public GetVasAirtimeTicketResponse GetTicket(String ticketid, String status, String authCredentials, int perpage,
            int requestedpage, String date,String transactionreference) {
        GetVasAirtimeTicketResponse ptr = new GetVasAirtimeTicketResponse();
        Merchantkey merchantkey = new Validator().ValidateAPiCall(authCredentials);
        if (merchantkey == null) {
            return new GetVasAirtimeTicketResponse(ResponseCodes.NS1);
        }

        if (perpage == 0) {
            perpage = DEFAULT_PER_PAGE_COUNT;
        }
        if (requestedpage == 0) {
            requestedpage = DEFAULT_PAGE;
        }
        TicketPersistence persistence = new TicketPersistence();
        TranslatePersistence tp = new TranslatePersistence();
        TicketPersistence.VasairtimeticketList chargeList = persistence.GetTicketById(ticketid, merchantkey,
                perpage, requestedpage, status, date,transactionreference);
        if (chargeList != null) {
            ptr.setCode(ResponseCodes.N00.getCode());
            ptr.setMessage(ResponseCodes.N00.getMessage());

            List<Vasairtimeticket> transactions = chargeList.transactions;
            transactions.forEach((trx) -> {
                VasAirtimeTicketRecord airtimeTicketRecord = new VasAirtimeTicketRecord(trx);
                log.info("getting ticket responses now");

                List<Vasairtimeticketresponse> list = persistence.GetTicketResponsesByTicketId(trx.getId() + "");
                log.info("response size=" + list.size());
                if (!list.isEmpty()) {
                    for (Vasairtimeticketresponse vasairtimeticketresponse : list) {

                        String userid = vasairtimeticketresponse.getUserid() + "";
                        log.info("response userid=" + userid);
                        List<MerchantDetails> detailses = tp.GetFullMerchantDetailsByToken(userid);
                        if (!detailses.isEmpty()) {
                            airtimeTicketRecord.getResponses().add(new VasAirtimeTicketRespRecord(vasairtimeticketresponse, detailses.get(0).getUsername()));
                        }
                    }

                }

                ptr.getTickets().add(airtimeTicketRecord);
            });
            ptr.getMetadata().setTotalcount(chargeList.totalcount);
            ptr.getMetadata().setPerpage(perpage);
            ptr.getMetadata().setPage(requestedpage);
            ptr.getMetadata().setPagecount(chargeList.transactions.size());
        }

        return ptr;
    }

    public CreateVasTicketResponse CreateTicketResponse(CreateVasTicketResRequest request, String authCredentials) {
        CreateVasTicketResponse createVasTicketResponse = new CreateVasTicketResponse();
        Merchantkey merchantkey = new Validator().ValidateAPiCall(authCredentials);
        if (merchantkey == null) {
            return new CreateVasTicketResponse(ResponseCodes.NS1);
        }
        TicketPersistence persistence = new TicketPersistence();
        List<Vasairtimeticket> vasairtimetickets = persistence.GetTicketByTicketId(request.getResponse().getTicketid());
        if (!vasairtimetickets.isEmpty()) {
            Vasairtimeticket vasairtimeticket = vasairtimetickets.get(0);
            Vasairtimeticketresponse vasairtimeticketresponse = new Vasairtimeticketresponse();
            vasairtimeticketresponse.setDescription(request.getResponse().getDescription());
            vasairtimeticketresponse.setTicketid(vasairtimeticket.getId());
            vasairtimeticketresponse.setTimein(new Date());
            vasairtimeticketresponse.setUserid(merchantkey.getId());
            if (persistence.logGenericTransaction(vasairtimeticketresponse)) {
                createVasTicketResponse.setCode(ResponseCodes.N00.getCode());
                createVasTicketResponse.setMessage(ResponseCodes.N00.getMessage());
                createVasTicketResponse.setTicketid(request.getResponse().getTicketid());
            } else {
                createVasTicketResponse.setCode(ResponseCodes.NS7.getCode());
                createVasTicketResponse.setMessage("Sorry, your ticket could not be updated. Please try again");
            }
        } else {
            createVasTicketResponse.setCode(ResponseCodes.NS404.getCode());
            createVasTicketResponse.setMessage("Sorry, we could not find that ticket");
        }

        return createVasTicketResponse;
    }

    public BaseResponse CloseTicket(CloseVasTicketRequest request, String authCredentials) {
        BaseResponse closeTicketResponse = new BaseResponse();
        Merchantkey merchantkey = new Validator().ValidateAPiCall(authCredentials);
        if (merchantkey == null) {
            return new CreateVasTicketResponse(ResponseCodes.NS1);
        }
        TicketPersistence persistence = new TicketPersistence();
        List<Vasairtimeticket> vasairtimetickets = persistence.GetTicketByTicketId(request.getTicket().getTicketid());
        if (!vasairtimetickets.isEmpty()) {
            Vasairtimeticket vasairtimeticket = vasairtimetickets.get(0);
            vasairtimeticket.setTstatus("CLOSED");
            vasairtimeticket.setClosedat(new Date());
            vasairtimeticket.setClosedby(merchantkey.getId());

            if (persistence.logGenericTransaction(vasairtimeticket)) {
                closeTicketResponse.setCode(ResponseCodes.N00.getCode());
                closeTicketResponse.setMessage(ResponseCodes.N00.getMessage());
            } else {
                closeTicketResponse.setCode(ResponseCodes.NS7.getCode());
                closeTicketResponse.setMessage("Sorry, your ticket could not be updated. Please try again");
            }
        } else {
            closeTicketResponse.setCode(ResponseCodes.NS404.getCode());
            closeTicketResponse.setMessage("Sorry, we could not find that ticket");
        }

        return closeTicketResponse;
    }

}
