package com.cgw.dash.core.report.model;

import com.cgw.dao.Charge;
import com.cgw.dao.Payout;

public class Record {

    /**
     * @return the transactionfees
     */
    public String getTransactionfees() {
        return transactionfees;
    }

    /**
     * @param transactionfees the transactionfees to set
     */
    public void setTransactionfees(String transactionfees) {
        this.transactionfees = transactionfees;
    }

    /**
     * @return the ubacomission
     */
    public String getUbacomission() {
        return ubacomission;
    }

    /**
     * @param ubacomission the ubacomission to set
     */
    public void setUbacomission(String ubacomission) {
        this.ubacomission = ubacomission;
    }

    /**
     * @return the cgcomission
     */
    public String getCgcomission() {
        return cgcomission;
    }

    /**
     * @param cgcomission the cgcomission to set
     */
    public void setCgcomission(String cgcomission) {
        this.cgcomission = cgcomission;
    }

    /**
     * @return the tax
     */
    public String getTax() {
        return tax;
    }

    /**
     * @param tax the tax to set
     */
    public void setTax(String tax) {
        this.tax = tax;
    }

    /**
     * @return the senderaccount
     */
    public String getSenderaccount() {
        return senderaccount;
    }

    /**
     * @param senderaccount the senderaccount to set
     */
    public void setSenderaccount(String senderaccount) {
        this.senderaccount = senderaccount;
    }

    private String country;
    private String recipient;
    private String amount;
    private String currency;
    private String merchantreference;
    private String narration;
    private String internalreference;
    private String processorreference;
    private String responsecode;
    private String responsemessage;
    private String network;
    private String transactiontime;
    private String email;
    private String processorbalance;
    private String transactiontype;
    private String dorc;

    private String amountdebited;
    private String amountsettled;
    private String ipaddress;
    private String customerfirstname;
    private String customerlastname;
    private String processorresponsecode;
    private String processorresponsemessage, mask, bank;

    private String transactionfees, ubacomission, cgcomission, tax, senderaccount;

    /**
     * @return the customerfirstname
     */
    public String getCustomerfirstname() {
        return customerfirstname;
    }

    /**
     * @param customerfirstname the customerfirstname to set
     */
    public void setCustomerfirstname(String customerfirstname) {
        this.customerfirstname = customerfirstname;
    }

    /**
     * @return the customerlastname
     */
    public String getCustomerlastname() {
        return customerlastname;
    }

    /**
     * @param customerlastname the customerlastname to set
     */
    public void setCustomerlastname(String customerlastname) {
        this.customerlastname = customerlastname;
    }

    /**
     * @return the processorresponsecode
     */
    public String getProcessorresponsecode() {
        return processorresponsecode;
    }

    /**
     * @param processorresponsecode the processorresponsecode to set
     */
    public void setProcessorresponsecode(String processorresponsecode) {
        this.processorresponsecode = processorresponsecode;
    }

    /**
     * @return the processorresponsemessage
     */
    public String getProcessorresponsemessage() {
        return processorresponsemessage;
    }

    /**
     * @param processorresponsemessage the processorresponsemessage to set
     */
    public void setProcessorresponsemessage(String processorresponsemessage) {
        this.processorresponsemessage = processorresponsemessage;
    }

    /**
     * @return the amountdebited
     */
    public String getAmountdebited() {
        return amountdebited;
    }

    /**
     * @param amountdebited the amountdebited to set
     */
    public void setAmountdebited(String amountdebited) {
        this.amountdebited = amountdebited;
    }

    /**
     * @return the amountsettled
     */
    public String getAmountsettled() {
        return amountsettled;
    }

    /**
     * @param amountsettled the amountsettled to set
     */
    public void setAmountsettled(String amountsettled) {
        this.amountsettled = amountsettled;
    }

    /**
     * @return the transactiontype
     */
    public String getTransactiontype() {
        return transactiontype;
    }

    /**
     * @param transactiontype the transactiontype to set
     */
    public void setTransactiontype(String transactiontype) {
        this.transactiontype = transactiontype;
    }

    /**
     * @return the dorc
     */
    public String getDorc() {
        return dorc;
    }

    /**
     * @param dorc the dorc to set
     */
    public void setDorc(String dorc) {
        this.dorc = dorc;
    }

    public Record(Payout payout) {
        this.amount = payout.getAmount() + "";
        this.country = payout.getCountry();
        this.recipient = payout.getRecipient();
        this.currency = payout.getCurrency();
        this.merchantreference = payout.getMerchantreference();
        this.narration = payout.getNarration();
        this.internalreference = payout.getInternalreference();
        this.processorreference = payout.getProcessorreference();
        this.responsecode = payout.getResponsecode();
        this.responsemessage = payout.getResponsemessage();
        this.network = payout.getNetwork();
        this.transactiontime = payout.getTimein() + "";
        if (payout.getProcessorbalance() == null) {
            this.processorbalance = "N/A";
        } else {
            this.processorbalance = payout.getProcessorbalancecurrency() + payout.getProcessorbalance();
        }
        if (payout.getType() != null) {
            switch (payout.getType()) {
                case "MM_WALLET_ACCOUNT_PAYOUT": {
                    this.transactiontype = "W2B";
                    this.dorc = "C";
                }
                break;
                case "ACCOUNT_PAYOUT": {
                    this.transactiontype = "B2W";
                    this.dorc = "D";
                }
                break;
                case "PREPAID_PAYOUT": {
                    this.transactiontype = "PREPAID";
                    this.dorc = "D";
                }
                break;
            }
        }

        this.transactionfees = payout.getTransactionfees();
        this.ubacomission = payout.getUbacomission();
        this.cgcomission = payout.getCgcomission();
        this.tax = payout.getTax();
        this.senderaccount = payout.getSenderaccount();

    }

    public Record(Charge charge) {
        this.amount = charge.getAmount() + "";
        this.country = charge.getCountry();
        this.email = charge.getSourcenumber2();
        this.currency = charge.getCurrency();
        this.merchantreference = charge.getMerchantreference();
        this.narration = charge.getNarration();
        this.internalreference = charge.getInternalreference();
        this.processorreference = charge.getProcessorreference();
        this.responsecode = charge.getResponsecode();
        this.responsemessage = charge.getResponsemessage();
        this.network = charge.getNetwork();
        this.transactiontime = charge.getTimein() + "";
        this.amountsettled = charge.getAmount() + "";
        this.amountdebited = charge.getFee() + "";
        this.ipaddress = charge.getSourcenumber3() + "";
        this.customerfirstname = charge.getFirstname();
        this.customerlastname = charge.getLastname();
        this.mask = charge.getStatus4();
        this.bank = charge.getBank();

        this.processorresponsecode = charge.getProcessorresponsecode();
        this.processorresponsemessage = charge.getProcessorresponsemessage();

    }

    /**
     * @return the amount
     */
    public String getAmount() {
        return amount;
    }

    /**
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    /**
     * @return the currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * @return the internalreference
     */
    public String getInternalreference() {
        return internalreference;
    }

    /**
     * @return the merchantreference
     */
    public String getMerchantreference() {
        return merchantreference;
    }

    /**
     * @return the narration
     */
    public String getNarration() {
        return narration;
    }

    /**
     * @return the network
     */
    public String getNetwork() {
        return network;
    }

    /**
     * @return the processorreference
     */
    public String getProcessorreference() {
        return processorreference;
    }

    /**
     * @return the recipient
     */
    public String getRecipient() {
        return recipient;
    }

    /**
     * @return the responsecode
     */
    public String getResponsecode() {
        return responsecode;
    }

    /**
     * @return the responsemessage
     */
    public String getResponsemessage() {
        return responsemessage;
    }

    /**
     * @return the transactiontime
     */
    public String getTransactiontime() {
        return transactiontime;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(String amount) {
        this.amount = amount;
    }

    /**
     * @param country the country to set
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * @param currency the currency to set
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    /**
     * @param internalreference the internalreference to set
     */
    public void setInternalreference(String internalreference) {
        this.internalreference = internalreference;
    }

    /**
     * @param merchantreference the merchantreference to set
     */
    public void setMerchantreference(String merchantreference) {
        this.merchantreference = merchantreference;
    }

    /**
     * @param narration the narration to set
     */
    public void setNarration(String narration) {
        this.narration = narration;
    }

    /**
     * @param network the network to set
     */
    public void setNetwork(String network) {
        this.network = network;
    }

    /**
     * @param processorreference the processorreference to set
     */
    public void setProcessorreference(String processorreference) {
        this.processorreference = processorreference;
    }

    /**
     * @param recipient the recipient to set
     */
    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    /**
     * @param responsecode the responsecode to set
     */
    public void setResponsecode(String responsecode) {
        this.responsecode = responsecode;
    }

    /**
     * @param responsemessage the responsemessage to set
     */
    public void setResponsemessage(String responsemessage) {
        this.responsemessage = responsemessage;
    }

    /**
     * @param transactiontime the transactiontime to set
     */
    public void setTransactiontime(String transactiontime) {
        this.transactiontime = transactiontime;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the processorbalance
     */
    public String getProcessorbalance() {
        return processorbalance;
    }

    /**
     * @param processorbalance the processorbalance to set
     */
    public void setProcessorbalance(String processorbalance) {
        this.processorbalance = processorbalance;
    }

    /**
     * @return the ipaddress
     */
    public String getIpaddress() {
        return ipaddress;
    }

    /**
     * @param ipaddress the ipaddress to set
     */
    public void setIpaddress(String ipaddress) {
        this.ipaddress = ipaddress;
    }

    /**
     * @return the mask
     */
    public String getMask() {
        return mask;
    }

    /**
     * @param mask the mask to set
     */
    public void setMask(String mask) {
        this.mask = mask;
    }

    /**
     * @return the bank
     */
    public String getBank() {
        return bank;
    }

    /**
     * @param bank the bank to set
     */
    public void setBank(String bank) {
        this.bank = bank;
    }

}
