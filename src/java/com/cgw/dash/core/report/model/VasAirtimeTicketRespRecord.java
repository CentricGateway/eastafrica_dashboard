package com.cgw.dash.core.report.model;

import com.cgw.dao.Vasairtimeticketresponse;

public class VasAirtimeTicketRespRecord {

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @return the time
     */
    public String getTime() {
        return time;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @param time the time to set
     */
    public void setTime(String time) {
        this.time = time;
    }

    private String id;
    private String description;
    private String time;
    private String by;
       private String transactionreference;

    public VasAirtimeTicketRespRecord(Vasairtimeticketresponse vasairtimeticketresponse, String by) {
        this.id = vasairtimeticketresponse.getId() + "";
        this.description = vasairtimeticketresponse.getDescription();
        this.time = vasairtimeticketresponse.getTimein() + "";
        this.by = by;
        this.transactionreference=vasairtimeticketresponse.getTransactionreference();
    }

    /**
     * @return the by
     */
    public String getBy() {
        return by;
    }

    /**
     * @param by the by to set
     */
    public void setBy(String by) {
        this.by = by;
    }

    /**
     * @return the transactionreference
     */
    public String getTransactionreference() {
        return transactionreference;
    }

    /**
     * @param transactionreference the transactionreference to set
     */
    public void setTransactionreference(String transactionreference) {
        this.transactionreference = transactionreference;
    }

}
