package com.cgw.dash.core.report.model;

import com.cgw.dao.Submerchant;

public class SubMerchantRecord {
    
     public SubMerchantRecord(Submerchant submerchant) {
        this.name = submerchant.getName()+ "";
        this.code = submerchant.getCode();
        this.businessname = submerchant.getBusinessname();

        this.website = submerchant.getWebsite();
        this.created = submerchant.getCreatedon()+"";
        

    }

     

    private String name;
    private String code;
    private String businessname;
    private String website;
    private String created;

    /**
     * @return the businessname
     */
    public String getBusinessname() {
        return businessname;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @return the created
     */
    public String getCreated() {
        return created;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the website
     */
    public String getWebsite() {
        return website;
    }

    /**
     * @param businessname the businessname to set
     */
    public void setBusinessname(String businessname) {
        this.businessname = businessname;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @param created the created to set
     */
    public void setCreated(String created) {
        this.created = created;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @param website the website to set
     */
    public void setWebsite(String website) {
        this.website = website;
    }

   
   
    

}
