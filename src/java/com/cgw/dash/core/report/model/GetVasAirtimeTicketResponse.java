package com.cgw.dash.core.report.model;

import com.cgw.service.response.model.BaseResponse;
import com.cgw.utils.ResponseCodes;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;

public class GetVasAirtimeTicketResponse extends BaseResponse {

    public GetVasAirtimeTicketResponse(ResponseCodes codes) {
        super(codes);
    }

    public GetVasAirtimeTicketResponse() {

    }

    @SerializedName("_metadata")
    @Expose
    private Metadata metadata = new Metadata();
    @SerializedName("tickets")
    @Expose
    private List<VasAirtimeTicketRecord> tickets = new ArrayList<>();

    public Metadata getMetadata() {
        return metadata;
    }

    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }

    /**
     * @return the tickets
     */
    public List<VasAirtimeTicketRecord> getTickets() {
        return tickets;
    }

    /**
     * @param tickets the tickets to set
     */
    public void setTickets(List<VasAirtimeTicketRecord> tickets) {
        this.tickets = tickets;
    }

}
