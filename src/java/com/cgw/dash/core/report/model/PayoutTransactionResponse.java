package com.cgw.dash.core.report.model;

import com.cgw.service.response.model.BaseResponse;
import com.cgw.utils.ResponseCodes;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;

public class PayoutTransactionResponse extends BaseResponse {

    public PayoutTransactionResponse(ResponseCodes codes) {
        super(codes);
    }

    public PayoutTransactionResponse() {

    }

    @SerializedName("_metadata")
    @Expose
    private Metadata metadata=new Metadata();
    @SerializedName("records")
    @Expose
    private List<Record> records = new ArrayList<>();

    public Metadata getMetadata() {
        return metadata;
    }

    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }

    public List<Record> getRecords() {
        return records;
    }

    public void setRecords(List<Record> records) {
        this.records = records;
    }

}
