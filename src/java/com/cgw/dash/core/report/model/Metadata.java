
package com.cgw.dash.core.report.model;

import java.util.ArrayList;
import java.util.List;
 
public class Metadata {

    /**
     * @return the links
     */
    public List<Link> getLinks() {
        return links;
    }

    /**
     * @return the page
     */
    public Integer getPage() {
        return page;
    }

    /**
     * @return the pagecount
     */
    public long getPagecount() {
        return pagecount;
    }

    /**
     * @return the perpage
     */
    public Integer getPerpage() {
        return perpage;
    }

    /**
     * @return the totalcount
     */
    public long getTotalcount() {
        return totalcount;
    }

    /**
     * @param links the links to set
     */
    public void setLinks(List<Link> links) {
        this.links = links;
    }

    /**
     * @param page the page to set
     */
    public void setPage(Integer page) {
        this.page = page;
    }

    /**
     * @param pagecount the pagecount to set
     */
    public void setPagecount(long pagecount) {
        this.pagecount = pagecount;
    }

    /**
     * @param perpage the perpage to set
     */
    public void setPerpage(Integer perpage) {
        this.perpage = perpage;
    }

    /**
     * @param totalcount the totalcount to set
     */
    public void setTotalcount(long totalcount) {
        this.totalcount = totalcount;
    }

    private Integer page;
    private Integer perpage;
    private long pagecount;
    private long totalcount;
    private List<Link> links = new ArrayList<>();

    
}
