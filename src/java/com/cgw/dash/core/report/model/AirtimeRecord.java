package com.cgw.dash.core.report.model;

import com.cgw.dao.Vasairtime;

public class AirtimeRecord {

    /**
     * @return the amount
     */
    public String getAmount() {
        return amount;
    }

    /**
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    /**
     * @return the currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * @return the internalreference
     */
    public String getInternalreference() {
        return internalreference;
    }

    /**
     * @return the merchantreference
     */
    public String getMerchantreference() {
        return merchantreference;
    }

    /**
     * @return the operator
     */
    public String getOperator() {
        return operator;
    }

    /**
     * @return the processorreference
     */
    public String getProcessorreference() {
        return processorreference;
    }

    /**
     * @return the recipient
     */
    public String getRecipient() {
        return recipient;
    }

    /**
     * @return the responsecode
     */
    public String getResponsecode() {
        return responsecode;
    }

    /**
     * @return the responsemessage
     */
    public String getResponsemessage() {
        return responsemessage;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @return the transactiontime
     */
    public String getTransactiontime() {
        return transactiontime;
    }

    /**
     * @return the transactiontype
     */
    public String getTransactiontype() {
        return transactiontype;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(String amount) {
        this.amount = amount;
    }

    /**
     * @param country the country to set
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * @param currency the currency to set
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    /**
     * @param internalreference the internalreference to set
     */
    public void setInternalreference(String internalreference) {
        this.internalreference = internalreference;
    }

    /**
     * @param merchantreference the merchantreference to set
     */
    public void setMerchantreference(String merchantreference) {
        this.merchantreference = merchantreference;
    }

    /**
     * @param operator the operator to set
     */
    public void setOperator(String operator) {
        this.operator = operator;
    }

    /**
     * @param processorreference the processorreference to set
     */
    public void setProcessorreference(String processorreference) {
        this.processorreference = processorreference;
    }

    /**
     * @param recipient the recipient to set
     */
    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    /**
     * @param responsecode the responsecode to set
     */
    public void setResponsecode(String responsecode) {
        this.responsecode = responsecode;
    }

    /**
     * @param responsemessage the responsemessage to set
     */
    public void setResponsemessage(String responsemessage) {
        this.responsemessage = responsemessage;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @param transactiontime the transactiontime to set
     */
    public void setTransactiontime(String transactiontime) {
        this.transactiontime = transactiontime;
    }

    /**
     * @param transactiontype the transactiontype to set
     */
    public void setTransactiontype(String transactiontype) {
        this.transactiontype = transactiontype;
    }

    public AirtimeRecord(Vasairtime vasairtime) {
        this.country = vasairtime.getCountry();
        this.recipient = vasairtime.getMobile();
        this.amount = vasairtime.getAmount() + "";
        this.currency = vasairtime.getCurrency();
        this.merchantreference = vasairtime.getMerchantreference();
        this.internalreference = vasairtime.getUniquereference();
        this.processorreference = vasairtime.getApitransactionid();
        this.responsecode = vasairtime.getStatus();
        this.status = vasairtime.getCode();
        this.responsemessage = vasairtime.getMessage();
        this.operator = vasairtime.getOperatorname();
        this.transactiontime = vasairtime.getTimein()+"";
        this.transactiontype = vasairtime.getTransactiontype();
    }

    private String country;
    private String recipient;
    private String amount;
    private String currency;
    private String merchantreference;
    private String internalreference;
    private String processorreference;
    private String responsecode;
    private String status;
    private String responsemessage;
    private String operator;
    private String transactiontime;
    private String transactiontype;

}
