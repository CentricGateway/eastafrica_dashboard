package com.cgw.dash.core.report.model;

import com.cgw.dao.Vasairtimeticket;
import java.util.ArrayList;
import java.util.List;

public class VasAirtimeTicketRecord {

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @return the ticketid
     */
    public String getTicketid() {
        return ticketid;
    }

    /**
     * @return the time
     */
    public String getTime() {
        return time;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @param ticketid the ticketid to set
     */
    public void setTicketid(String ticketid) {
        this.ticketid = ticketid;
    }

    /**
     * @param time the time to set
     */
    public void setTime(String time) {
        this.time = time;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    public VasAirtimeTicketRecord(Vasairtimeticket vasairtime) {
        this.description = vasairtime.getDescription();
        this.time = vasairtime.getTimein() + "";
        this.title = vasairtime.getTitle();
        this.status = vasairtime.getTstatus();
        this.ticketid = vasairtime.getUniqueid();
        this.transactionreference = vasairtime.getTransactionreference();

    }

    private String ticketid;
    private String transactionreference;
    private String title;
    private String description;
    private String time;
    private String status;
    private List<VasAirtimeTicketRespRecord> responses = new ArrayList<>();

    /**
     * @return the responses
     */
    public List<VasAirtimeTicketRespRecord> getResponses() {
        return responses;
    }

    /**
     * @param responses the responses to set
     */
    public void setResponses(List<VasAirtimeTicketRespRecord> responses) {
        this.responses = responses;
    }

    /**
     * @return the transactionreference
     */
    public String getTransactionreference() {
        return transactionreference;
    }

    /**
     * @param transactionreference the transactionreference to set
     */
    public void setTransactionreference(String transactionreference) {
        this.transactionreference = transactionreference;
    }

}
