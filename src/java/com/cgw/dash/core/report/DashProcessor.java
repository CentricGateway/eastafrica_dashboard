/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cgw.dash.core.report;

import com.cgw.core.persist.ChargePersistence;
import com.cgw.core.persist.MerchantPersistence;
import com.cgw.core.persist.PayoutPersistence;
import com.cgw.dao.Charge;
import com.cgw.dao.Merchantinfo;
import com.cgw.dao.Merchantkey;
import com.cgw.dao.Merchantuser;
import com.cgw.dao.Payout;
import com.cgw.dao.Submerchant;
import com.cgw.dao.Vasairtime;
import com.cgw.dash.core.report.model.AirtimeRecord;
import com.cgw.dash.core.report.model.AirtimeTransactionResponse;
import com.cgw.dash.core.report.model.GetSubmerchantsResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.cgw.dash.core.report.model.PayoutTransactionResponse;
import com.cgw.dash.core.report.model.Record;
import com.cgw.dash.core.report.model.SubMerchantRecord;
import com.cgw.utils.ResponseCodes;
import java.util.List;
 
public class DashProcessor {

    private static final Logger LOG = LogManager.getLogger(DashProcessor.class);

    private final int DEFAULT_PER_PAGE_COUNT = 20;
    private final int DEFAULT_PAGE = 0;

    public PayoutTransactionResponse GetPayoutTransactions(Merchantkey merchantkey, int perpage, int requestedpage,String date,
             String internalreference, String merchantreference, String recipient, String status, String currency, String todate) {

        PayoutTransactionResponse ptr = new PayoutTransactionResponse();
        if (perpage == 0) {
            perpage = DEFAULT_PER_PAGE_COUNT;
        }
        if (requestedpage == 0) {
            requestedpage = DEFAULT_PAGE;
        }
        MerchantPersistence merchantPersistence = new MerchantPersistence();
        Merchantinfo merchantinfo = merchantPersistence.GetMerchantByMerchantKey(merchantkey);

        Merchantuser merchantuser = merchantPersistence.GetMerchantUserByMerchantKey(merchantkey);

        PayoutPersistence.PayoutList payoutList = new PayoutPersistence().GetPayoutTransactions(merchantinfo.getId(), perpage,
                requestedpage, merchantuser.getDefaultcountry(),date,  internalreference,  merchantreference,  recipient,  status,  currency,  todate);
        if (payoutList != null) {
            ptr.setCode(ResponseCodes.N00.getCode());
            ptr.setMessage(ResponseCodes.N00.getMessage());

            List<Payout> transactions = payoutList.transactions;
            transactions.forEach((payout) -> {
                ptr.getRecords().add(new Record(payout));
            });
            ptr.getMetadata().setTotalcount(payoutList.totalcount);
            ptr.getMetadata().setPerpage(perpage);
            ptr.getMetadata().setPage(requestedpage);
            ptr.getMetadata().setPagecount(payoutList.transactions.size());
        }

        return ptr;

    }

    public PayoutTransactionResponse GetChargeTransactions(Merchantkey merchantkey, int perpage, int requestedpage, String date, String merchantreference, String internalreference,
            String currency, String cardtype, String transactionstatus,String transactiontype,String todate) {

        PayoutTransactionResponse ptr = new PayoutTransactionResponse();
        if (perpage == 0) {
            perpage = DEFAULT_PER_PAGE_COUNT;
        }
        if (requestedpage == 0) {
            requestedpage = DEFAULT_PAGE;
        }
        ChargePersistence.ChargeList chargeList = new ChargePersistence().GetChargeTransactions(merchantkey, perpage,
                requestedpage,  date,  merchantreference,  internalreference,
             currency,  cardtype,  transactionstatus, transactiontype,todate);
        if (chargeList != null) {
            ptr.setCode(ResponseCodes.N00.getCode());
            ptr.setMessage(ResponseCodes.N00.getMessage());

            List<Charge> transactions = chargeList.transactions;
            transactions.forEach((charge) -> {
                ptr.getRecords().add(new Record(charge));
            });
            ptr.getMetadata().setTotalcount(chargeList.totalcount);
            ptr.getMetadata().setPerpage(perpage);
            ptr.getMetadata().setPage(requestedpage);
            ptr.getMetadata().setPagecount(chargeList.transactions.size());
        }

        return ptr;

    }

    public AirtimeTransactionResponse GetAirtimeTransactions(Merchantkey merchantkey, int perpage,
            int requestedpage, String date,
            String recipient,
            String merchantreference,
            String internalreference,
            String processorreference,
            String responsecode,
            String status,
            String operator) {

        AirtimeTransactionResponse ptr = new AirtimeTransactionResponse();
        if (perpage == 0) {
            perpage = DEFAULT_PER_PAGE_COUNT;
        }
        if (requestedpage == 0) {
            requestedpage = DEFAULT_PAGE;
        }
        ChargePersistence.AirtimeList chargeList = new ChargePersistence().GetAirtimeTransactions(merchantkey, perpage,
                requestedpage, date,
                recipient,
                merchantreference,
                internalreference,
                processorreference,
                responsecode,
                status,
                operator);
        if (chargeList != null) {
            ptr.setCode(ResponseCodes.N00.getCode());
            ptr.setMessage(ResponseCodes.N00.getMessage());

            List<Vasairtime> transactions = chargeList.transactions;
            transactions.forEach((trx) -> {
                ptr.getRecords().add(new AirtimeRecord(trx));
            });
            ptr.getMetadata().setTotalcount(chargeList.totalcount);
            ptr.getMetadata().setPerpage(perpage);
            ptr.getMetadata().setPage(requestedpage);
            ptr.getMetadata().setPagecount(chargeList.transactions.size());
        }

        return ptr;

    }

    public GetSubmerchantsResponse GetSubMerchants(Merchantinfo merchantkey, int perpage, int requestedpage) {

        GetSubmerchantsResponse ptr = new GetSubmerchantsResponse();
        if (perpage == 0) {
            perpage = DEFAULT_PER_PAGE_COUNT;
        }
        if (requestedpage == 0) {
            requestedpage = DEFAULT_PAGE;
        }
        ChargePersistence.SubmerchantList chargeList = new ChargePersistence().GetSubmerchants(merchantkey, perpage,
                requestedpage);
        if (chargeList != null) {
            ptr.setCode(ResponseCodes.N00.getCode());
            ptr.setMessage(ResponseCodes.N00.getMessage());

            List<Submerchant> transactions = chargeList.transactions;
            transactions.forEach((charge) -> {
                ptr.getRecords().add(new SubMerchantRecord(charge));
            });
            ptr.getMetadata().setTotalcount(chargeList.totalcount);
            ptr.getMetadata().setPerpage(perpage);
            ptr.getMetadata().setPage(requestedpage);
            ptr.getMetadata().setPagecount(chargeList.transactions.size());
        }

        return ptr;

    }

}
