/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cgw.utils;

import java.util.HashMap;
import java.util.Map;

 
public enum SourceOperations {
    MMCHARGE("mm-charge", "Mobile Money Charge"),
    MMPAYOUT("mm-payout", "Mobile Money Payout"),
    MMENQUIRY("mm-enquiry", "Mobile Money Enquiry"),
    S9("S9", "Invalid Operation");

    private static final Map<String, String> codeMap = new HashMap<>();

    static {
        for (SourceOperations rc : SourceOperations.values()) {
            codeMap.put(rc.code, rc.name());
        }
    }

    /**
     * Numerical return code.
     */
    private String code = "";

    /**
     * Description of return code and possible corrective actions.
     */
    private String message = "";

    SourceOperations(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    /**
     * Returns the ErrorCode using the numerical code.
     *
     * @param code int
     * @return ErrorCode if code matches, null otherwise.
     */
    public static SourceOperations getByCode(String code) {
        if (codeMap.containsKey(code)) {
            return SourceOperations.valueOf(codeMap.get(code));
        } else {

            return SourceOperations.valueOf(codeMap.get("S9"));
        }

    }

}
