/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cgw.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
 
public class DateUtils {
    public static String GetDateFcubs() {

        //2015-03-11
        Calendar now = Clock.getCalendarInstance();
        String currentYear = String.valueOf(now.get(Calendar.YEAR));
        String currentMonth = String.format("%02d", now.get(Calendar.MONTH) + 1);
        String currentDay = String.format("%02d", now.get(Calendar.DAY_OF_MONTH));
        String date = currentYear + "-" + currentMonth + "-" + currentDay;

        return date;

    }
    public static String ValidateDate(String dateStr) {

        String newDate = "";
        try {
            DateFormat fromFormat = new SimpleDateFormat("yyyy-MM-dd");
            fromFormat.setLenient(false);
            Date date = fromFormat.parse(dateStr);

            newDate = new SimpleDateFormat("yyyy-MM-dd").format(date);
        } catch (ParseException ex) {
            System.out.println("err" + ex.getMessage());
        }
        return newDate;
    }
 public static String ValidateDateWithFullMonth(String dateStr) {

        String newDate = "";//25-APR-89
        try {
            DateFormat fromFormat = new SimpleDateFormat("dd-MMM-yy");
            fromFormat.setLenient(false);
            Date date = fromFormat.parse(dateStr);

            newDate = new SimpleDateFormat("yyyy-MM-dd").format(date);
        } catch (ParseException ex) {
            System.out.println("err" + ex.getMessage());
        }
        return newDate;
    }
    
     public static String GetNowFullDate() {
          return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
    }
     public static String GetImpalaNowFullDate() {
          return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(new Date())+"+01:00";
    }//yyyy-MM-dd'T'HH:mm:ssZ
     
     public static void main(String[] args) {
         System.out.println(GetImpalaNowFullDate());
    }
     
     
    
}
