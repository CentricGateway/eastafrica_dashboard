/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cgw.utils;

import java.util.HashMap;
import java.util.Map;

 
public enum ChargeCountry {
    UG("UG", "Uganda"),
    S9("S9", "Invalid Country");

    private static final Map<String, String> codeMap = new HashMap<>();

    static {
        for (ChargeCountry rc : ChargeCountry.values()) {
            codeMap.put(rc.code, rc.name());
        }
    }

    /**
     * Numerical return code.
     */
    private String code = "";

    /**
     * Description of return code and possible corrective actions.
     */
    private String name = "";

    ChargeCountry(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    

    /**
     * Returns the ErrorCode using the numerical code.
     *
     * @param code int
     * @return ErrorCode if code matches, null otherwise.
     */
    public static ChargeCountry getByCode(String code) {
        if (codeMap.containsKey(code)) {
            return ChargeCountry.valueOf(codeMap.get(code));
        } else {

            return ChargeCountry.valueOf(codeMap.get("S9"));
        }

    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

}
