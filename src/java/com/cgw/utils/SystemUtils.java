/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cgw.utils;

import java.util.UUID;
import org.apache.logging.log4j.core.util.UuidUtil;

 
public class SystemUtils {
    
    public enum SystemProp {

        ACCESS_NIP_ENABLE,ACCESS_ETRANZACT_ENABLE,NIP_UP,CBA_UP,BVN_UP, PAYDAY_UP 

    }
     public enum SystemPropState {

        NOT_VALIDATED, VALIDATED

    }
     public static String GenerateUuid() {
        UUID nonce = UUID.randomUUID();
        return "SB" + nonce.toString().replaceAll("-", "");
    }
     
     public static void main(String[] args) {
        
         System.out.println( UuidUtil.getTimeBasedUuid().toString().replace("-", ""));
         System.out.println(UUID.randomUUID());
    }
     
     
    
    
}
