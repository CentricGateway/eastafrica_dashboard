/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cgw.utils;

import java.util.HashMap;
import java.util.Map;

 
public enum ResponseCodes {
    N00("00", "Successful"),
    NS1("S1", "Invalid credentials"),
    NS2("S2", "Invalid Amount"),
    NS3("S3", "Transaction not permitted to merchant"),
    NS4("S4", "Invalid Currency"),
    NS5("S5", "Invalid/Missing parameters"),
    NS6("S6", "Invalid Country"),
    NS7("S7", "Generic Error Occurred"),
    NS8("S8", "Null/Missing publickey"),
    NS10("S10", "Null/Missing authentication token"),
    NS11("S11", "Unable to connect to destination"),
    NS12("S12", "Transaction Failed"),
    NS13("S13", "Invalid payout type"),
    NS14("S14", "Transaction reference must be unique"),
    NS404("S404", "No Data Found"),
    NS15("S15", "The credentials you supplied was not valid. Please try again"),
    NS16("S16", "Your account has not been activated. Please check your email for an activation link and try again"),
    NS17("S17", "Your account has been disabled. Please contact the system administrator");

    private static final Map<String, String> codeMap = new HashMap<>();

    static {
        for (ResponseCodes rc : ResponseCodes.values()) {
            codeMap.put(rc.code, rc.name());
        }
    }

    /**
     * Numerical return code.
     */
    private String code = "";

    /**
     * Description of return code and possible corrective actions.
     */
    private String message = "";

    ResponseCodes(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    /**
     * Returns the ErrorCode using the numerical code.
     *
     * @param code int
     * @return ErrorCode if code matches, null otherwise.
     */
    public static ResponseCodes getByCode(String code) {
        if (codeMap.containsKey(code)) {
            return ResponseCodes.valueOf(codeMap.get(code));
        } else {

            return ResponseCodes.valueOf(codeMap.get("NXX"));
        }

    }

}
