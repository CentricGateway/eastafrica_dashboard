/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cgw.utils;

import java.math.BigDecimal;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.BigDecimalValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author toba
 */
public class GenericUtils {

    private static final Logger log = LogManager.getLogger(GenericUtils.class);

    public static String ValidateSet(HashMap params) {
        String resp = "";
        Set set = params.entrySet();
        Iterator i = set.iterator();
        while (i.hasNext()) {
            Map.Entry me = (Map.Entry) i.next();
            if (StringUtils.isEmpty(String.valueOf(me.getValue()))
                    || StringUtils.contains(String.valueOf(me.getValue()), "null")) {
                resp = "Sorry,you need to add " + me.getKey() + " to your request!";
                break;
            }
        }
        return resp;
    }
 
    public static String GetSHA512String(String inp) {
        StringBuffer hexString = new StringBuffer();
        try {
            MessageDigest digest = java.security.MessageDigest.getInstance("SHA-512");
            digest.update(inp.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            for (int i = 0; i < messageDigest.length; i++) {
                String h = Integer.toHexString(0xFF & messageDigest[i]);
                while (h.length() < 2) {
                    h = "0" + h;
                }
                hexString.append(h);
            }

        } catch (NoSuchAlgorithmException ex) {
           log.info(ex,ex);
        }
        return hexString.toString();
    }

     

    public static String GetBase64EncodeString(String input) {
        byte[] encodedBytes = Base64.encodeBase64(input.getBytes());
        return new String(encodedBytes);
    }
    
    public static String CleanXml(String input) {
        String ret = "";
        if (StringUtils.isNotBlank(input)) {
            ret = input.replaceAll("[&]", "&amp;");

        }
        return ret;

    }
    

}
